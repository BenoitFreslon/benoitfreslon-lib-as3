﻿package com.benoitfreslon.fx
{
	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.0
	 */
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;

	public class Shake extends Sprite
	{
		
		private var mc:DisplayObjectContainer;
		private var t:Number;
		private var amplitude:Number;
		private var attenuation:Number;
		private var oldX:Number;
		private var oldY:Number;
		private var directionX:int;
		private var directionY:int;
		
		public function Shake(pMc:DisplayObjectContainer):void {
			mc = pMc;
			oldX = mc.x
			oldY = mc.y
			mc.addChild(this);
		}
		public function start(pAmplitude:Number = 5, pAttenuation:Number = .9):void {
			
			mc.x = oldX;
			mc.y = oldY;
			
			amplitude = pAmplitude;
			attenuation = pAttenuation;
			t = 0;
			addEventListener(Event.ENTER_FRAME, enterFrame);
			mc.addEventListener(Event.REMOVED_FROM_STAGE, removed);
			randomDirection();
		}
		private function enterFrame(pEvt:Event):void {
			if (amplitude == 0) {
				stop();
			} else {
				mc.x = oldX + directionX * amplitude;
				mc.y = oldY + directionY * amplitude;
				amplitude *= attenuation;
				randomDirection()
			}
		}
		private function randomDirection():void {
			directionX = Math.round(Math.random() * -2 + 1);
			directionY = Math.round(Math.random() * -2 + 1);
		}
		public function stop():void {
			mc.x = oldX;
			mc.y = oldY;
			removeEventListener(Event.ENTER_FRAME, enterFrame);
			mc.removeEventListener(Event.REMOVED_FROM_STAGE, removed);
		}
		private function removed(pEvt:Event ):void 
		{
			stop();
		}
	}
	
}
﻿package com.benoitfreslon {
	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.0
	 */
	import flash.display.MovieClip;
	import flash.events.Event;
	public class FrameClock extends FrameTimer {
		
		function FrameClock (pMc:MovieClip, pFc:Function, pTime:uint, pParam:* = null) {
			super(pMc, pFc, pTime, pParam);
		}
		override public function end():void {
			//trace("Timer === end Timer");
			if (param) {
				fc(param);
			} else {
				fc();
			}
			t = 0;
		}
	}
}
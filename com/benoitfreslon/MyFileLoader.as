﻿package com.benoitfreslon
{
	
	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.05
	 */
	
	 
    import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.display.Loader;
    import flash.net.URLRequest;
	import flash.net.URLLoaderDataFormat;
	import flash.events.IEventDispatcher;
	import flash.events.HTTPStatusEvent;
	import flash.net.URLVariables;
	 
	public class  MyFileLoader
	{
		public var fcComplete:Function = function(pEvt:*):void{};
		public var fcError:Function = function():void{};

        public function MyFileLoader(pDownloadURL:String, pFcComplete:Function = null, pFcError:Function = null) {
			//trace("MyFileLoader: ", pDownloadURL, pFcComplete, pFcError);
			var loader:Loader = new Loader();
            configureListeners(loader.contentLoaderInfo);
			
			fcComplete = pFcComplete;
			fcError = pFcError;
			
			var request:URLRequest = new URLRequest(pDownloadURL);

            try {
                loader.load(request);
            } catch (error:Error) {
                trace("Unable to load requested document.");
				pFcError();
            }
        }
        private function configureListeners(dispatcher:IEventDispatcher):void {
            dispatcher.addEventListener(Event.COMPLETE, completeHandler);
            dispatcher.addEventListener(Event.OPEN, openHandler);
            dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
            dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
            dispatcher.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);
            dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
        }

        private function completeHandler(event:Event):void {
            //var loader:URLLoader = URLLoader(event.target);
			//trace("MyFileLoader: completeHandler: " + event);
			
			fcComplete(event);
			
        }

        private function openHandler(event:Event):void {
            //trace("openHandler: " + event);
        }

        private function progressHandler(event:ProgressEvent):void {
           //trace("progressHandler loaded:" + event.bytesLoaded + " total: " + event.bytesTotal);
        }

        private function securityErrorHandler(event:SecurityErrorEvent):void {
			//trace("securityErrorHandler: " + event);
			fcError();
        }

        private function httpStatusHandler(event:HTTPStatusEvent):void {
            //trace("httpStatusHandler: " + event);
        }

        private function ioErrorHandler(event:IOErrorEvent):void {
            //trace("ioErrorHandler: " + event);
			fcError();
        }

	}
}
﻿package com.benoitfreslon
{

	/**
	 * @private
	 * @copy
	 * @author Benoît Freslon
	 //* @version 1.10
	 */
	public class  MyArray extends Array
	{
		public function MyArray(...rest):void {
			//splice(this, [0, 0].concat(rest));
			super(rest);
			//super();
		}
		public static function executeMethod(pArr:Array, pMethod:String, ...pParams:*):void {
			for (var i:String in pArr) {
				pArr[i][pMethod].apply(null,pParams);
			}
		}
		public static function selectRandom(pArr:Array):* {
			return pArr[Math.ceil(Math.random() * pArr.length - 1)];
		}
		public static function selectRandomIndex(arr:Array):int {
			return Math.random() * arr.length
		}
		public static function remove(pArr:Array, pElement:*):Array {
			var i:int = pArr.length;
			var arr:Array = []
			while (i--) {
				if (pArr[i] != pElement) {
					arr.push(pArr[i]);
				}
			}
			return arr;
		}
		public static function removeOne(pArr:Array, pElement:*):Array {
			var i:int = pArr.length;
			var arr:Array = [];
			var found:Boolean = false;
			while (i--) {
				//trace(pArr[i], pElement, pArr[i] == pElement);
				if (!found && pArr[i] == pElement) {
					found = true;
					//trace("*** MyArray:",pElement, "found");
				} else {
					arr.push(pArr[i]);
				}
			}
			return arr;
		}
		public static function removeOneObject(pArr:Array, pProp:String, pValue:*):Array {
			var i:int = pArr.length;
			var arr:Array = [];
			var found:Boolean = false;
			if (pValue is Array) {
				while (i--) {
					//trace(pArr[i], pElement, pArr[i] == pElement);
					if (!found && String(pArr[i][pProp]) == String(pValue)) {
						found = true;
						//trace("*** MyArray:",pElement, "found");
					} else {
						arr.push(pArr[i]);
					}
				}
			} else {
				while (i--) {
					//trace(pArr[i], pElement, pArr[i] == pElement);
					if (!found && pArr[i][pProp] == pValue) {
						found = true;
						//trace("*** MyArray:",pElement, "found");
					} else {
						arr.push(pArr[i]);
					}
				}
			}
			return arr;
		}
		public static function removeObjects(pArr:Array, pProp:String, pValue:*):Array {
			var i:int = pArr.length;
			var arr:Array = [];
			if (pValue is Array) {
				while (i--) {
					//trace(pArr[i], pElement, pArr[i] == pElement);
					if (String(pArr[i][pProp]) == String(pValue)) {
						//trace("*** MyArray:",pElement, "found");
					} else {
						arr.push(pArr[i]);
					}
				}
			} else {
				while (i--) {
					//trace(pArr[i], pElement, pArr[i] == pElement);
					if (pArr[i][pProp] == pValue) {
						//trace("*** MyArray:",pElement, "found");
					} else {
						arr.push(pArr[i]);
					}
				}
			}
			return arr;
		}
		public static function getObjectFromProp(pArr:Array, pProp:String, pValue:*):Object {
			var i:String;
			if (pValue is Array) {
				for (i in pArr) {
					if (String(pArr[i][pProp]) == String(pValue)) {
						return pArr[i];
					}
				}
			} else {
				for (i in pArr) {
					if (pArr[i][pProp] == pValue) {
						return pArr[i];
					}
				}
			}
			return null;
		}
		public static function getNbItems(pArr:Array, pValue:*):int {
			var count:int = 0;
			var i:String;
			if (pValue is Array) {
				for (i in pArr) {
					if (String(pArr[i]) == String(pValue)) {
						count++;
					}
				}
			} else {
				for (i in pArr) {
					if (pArr[i]== pValue) {
						count++;
					}
				}
			}
			return count;
		}
		public static function propExist(pArr:Array, pProp:String, pValue:*):int {
			var i:String;
			if (pValue is Array) {
				for (i in pArr) {
					//trace("*** MyArray",i, pProp, pArr[i][pProp]+"=="+pValue);
					if (String(pArr[i][pProp]) == String(pValue)) {
						//trace("*** myArray: prop exists");
						return int(i);
					}
				}
			} else {
				for (i in pArr) {
					//trace("*** MyArray",i, pProp, pArr[i][pProp]+"=="+pValue);
					if (pArr[i][pProp] == pValue) {
						//trace("*** myArray: prop exists");
						return int(i);
					}
				}
			}
			return -1;
		}
		public static function shuffle (pArr:Array):Array {

			var i:int = pArr.length;
			var rand:int;
			var current:*;

			while ( i-- )
			{
				rand = Math.floor ( Math.random()*pArr.length );
				current = pArr[i];
				pArr[i] = pArr[rand];
				pArr[rand] = current;
			}
			return pArr;
		}
		public static function maxValue(pArr:Array):Number {
			//trace("MyArray: ", pArr);
			var max:Number = pArr[0];
			//trace(pArr[0]);
			//trace("max: " + max);
			var len:int = pArr.length;
			for (var i:int = 0; i < len; i++ ) {
				if (pArr[i] > max) {
					max = pArr[i];
				}
			}
			//trace("max: " + max);
			return max;
		}
		public static function minValue(pArr:Array):Number {
			var min:Number = pArr[0];
			var len:int = pArr.length;
			for (var i:int = 0; i < len; i++ ) {
				if (pArr[i] < min) {
					min = pArr[i];
				}
			}
			return min;
		}
		public static function maxIndex(pArr:Array):int {
			//trace("MyArray: ", pArr);
			var max:Number = pArr[0];
			var maxIndex:int = 0;
			var len:int = pArr.length;
			for (var i:int = 0; i < len; i++ ) {
				if (pArr[i] > max) {
					max = pArr[i];
					maxIndex = i;
				}
			}
			return int(maxIndex);
		}
		public static function minIndex(pArr:Array):int {
			var min:Number = pArr[0];
			var minIndex:int = 0;
			var len:int = pArr.length;
			for (var i:int = 0; i < len; i++ ) {
				if (pArr[i] < min) {
					min = pArr[i];
					minIndex = i;
				}
			}
			return int(minIndex);
		}

	}

}


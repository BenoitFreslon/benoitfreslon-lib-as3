package com.benoitfreslon
{
	import flash.display.DisplayObject;
	/**
	 * ...
	 * @author Benoit Freslon
	 * @version 1.0
	 */
	public class MyAchivements
	{
		private var root:DisplayObject;
		public static var on:Boolean = false;
		public static var instance:MyAchivements;
		
		private var _gameAchievements:Object = { };
		
		public function MyAchivements(pRoot:DisplayObject):void
		{
			root = pRoot;
			instance = this;
		}

		private function onConnectError(errorCode:String):void {
			trace("MyAchivements: onConnectError", errorCode);
			on = false;
		}
		private function onGameAchievements( ach:Array ):void
		{
			for( var i:int = 0; i < ach.length; i++ )
			{
				_gameAchievements[ ach[i].id ] = ach[i];
				_gameAchievements[ ach[i].id ].unlocked = false;
			}
		}
		private function onUserAchievements( ach:Array ):void
		{
			for( var i:int = 0; i < ach.length; i++ )
			{
				_gameAchievements[ ach[i].id ] = ach[i];
				_gameAchievements[ ach[i].id ].unlocked = true;
			}
		}
		private function onNewAchievement( ach:Object ):void
		{
			_gameAchievements[ ach.id ] = ach;
			_gameAchievements[ ach.id ].unlocked = true;
		}
		public function showAwards():void {
			if (!on) { return;	}
		}
	}
}


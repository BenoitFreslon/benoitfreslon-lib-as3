package com.benoitfreslon
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author Benoît Freslon
	 */
	public class MyScrollbar
	{
		/**
		 *
		 * @version 1.00
		 * @author Benoît Freslon
		 */
		
		private var mc:MovieClip;
		private var bar:MovieClip;
		private var mask:MovieClip;
		private var initY:Number;
		private var initX:Number;
		private var initMcY:Number;
		public var coefSmooth:Number = 5;
		
		public function MyScrollbar(pmc:MovieClip, pbar:MovieClip, pmask:MovieClip)
		{
			mc = pmc;
			bar = pbar;
			mask = pmask;
			
			bar.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			bar.buttonMode = true;
			
			bar.x = Math.floor(bar.x);
			bar.y = Math.floor(bar.y);
			
			initY = bar.y;
			initX = bar.x;
			
			mc.y = Math.floor(mc.y);
			
			initMcY = mc.y;
			
			bar.addEventListener(Event.ENTER_FRAME, enterFrame);
			bar.addEventListener(Event.REMOVED_FROM_STAGE, removed);
			mc.mask = mask;
			bar.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUp);
		
		}
		
		private function mouseUp(e:MouseEvent):void
		{
			bar.stopDrag();
			bar.gotoAndStop(1);
		}
		
		function mouseDown(e:MouseEvent):void
		{
			var rec:Rectangle = new Rectangle(initX, initY, 0, mask.height - bar.height);
			bar.startDrag(false, rec);
			bar.gotoAndStop(2);
		}
		
		function enterFrame(e:Event):void
		{
			var coef:Number = (bar.y-initY) / (mask.height - bar.height);
			trace("coef : " + coef);
			if (coef > 1)
			{
				coef = 1;
			}
			if (coef < 0)
			{
				coef = 0;
			}
			
			var targetY:Number = initMcY + coef * -(mc.height - mask.height);
			//trace( "initMcY : " + initMcY );
			//trace("targetY : " + targetY);
			mc.y += (targetY - mc.y) / coefSmooth;
		}
		
		function removed(e:Event):void
		{
			
			bar.removeEventListener(Event.ENTER_FRAME, enterFrame);
			bar.removeEventListener(Event.REMOVED_FROM_STAGE, removed);
			bar.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			bar.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
		}
	
	}

}
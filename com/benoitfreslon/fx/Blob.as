﻿package com.benoitfreslon.fx
{
	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.02
	 */
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;

	public class Blob extends Sprite
	{
		
		private var mc:Object;
		private var t:Number;
		private var amplitude:Number;
		private var speed:Number;
		private var attenuation:Number;
		private var oldScaleX:Number;
		private var oldScaleY:Number;
		
		public function Blob(pMc:Object):void {
			//trace("Blob: constructor");
			
			mc = pMc;
			//trace(mc.scaleX);
			oldScaleX = mc.scaleX;
			oldScaleY = mc.scaleY;
			
			mc.addChild(this);
		}
		/**
		 * 
		 * @param	pSpeed
		 * @param	pAmplitude
		 * @param	pAttenuation
		 */
		public function start(pSpeed:Number = .5, pAmplitude:Number = .2, pAttenuation:Number = .92):void {
			//trace("Blob: start",mc, pSpeed, pAmplitude, pAttenuation);
			mc.scaleX = oldScaleX;
			mc.scaleY = oldScaleY;
			
			speed = pSpeed;
			amplitude = pAmplitude;
			attenuation = pAttenuation;
			t = 0;
			addEventListener(Event.ENTER_FRAME, enterFrame);
			mc.addEventListener(Event.REMOVED_FROM_STAGE, removed);
		}
		public function initScale():void {
			
			oldScaleX = mc.scaleX;
			oldScaleY = mc.scaleY;
		}
		private function enterFrame(pEvt:Event):void {
			if (amplitude < 0.01) {
				stop();
			} else {
				mc.scaleX = oldScaleX+Math.cos(t) * amplitude;
				mc.scaleY = oldScaleY-Math.sin(t) * amplitude;
				t += speed;
				//trace(mc.scaleX);
				amplitude *= attenuation;
			}
		}
		public function stop():void {
			//trace("*** Blob: stop");
			//trace(oldScaleX);
			mc.scaleX = oldScaleX;
			mc.scaleY = oldScaleY;
			
			removeEventListener(Event.ENTER_FRAME, enterFrame);
			mc.removeEventListener(Event.REMOVED_FROM_STAGE, removed);
		}
		private function removed(pEvt:Event ):void 
		{
			stop();
		}
	}
	
}
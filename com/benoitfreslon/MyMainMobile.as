package com.benoitfreslon {
	CONFIG::mobile {
		import com.adobe.ane.social.SocialServiceType;
		import com.adobe.ane.social.SocialUI;
	}
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	
	/**
	 * ...
	 * @author Benoît Freslon
	 */
	public class MyMainMobile extends MyMainBase {
		
		protected var sUI : *;
		protected var msgFlag : Boolean;
		protected var urlFlag : Boolean;
		protected var addImage : Boolean = false;
		protected var bmd : BitmapData;
		
		public function MyMainMobile () {
			super ();
		
		}
		override public function clickPostOnFacebook (e : MouseEvent = null) : void {
			shareOnFacebook (socialMessage, gameURL);
		}
		
		override public function clickPostOnTwitter (e : MouseEvent = null) : void {
			shareOnTwitter (socialMessage, gameURL);
		}
		public function shareOnTwitter (msg : String = "" , pURL : String = null , pImage : BitmapData = null, pImageURL:String = null) : void {
			//super.postOnTwitter(msg, pURL, pImage);
			launchTweetUI (msg , pURL , pImage, pImageURL)
		}
		
		public function shareOnFacebook (msg : String = "" , pURL : String = null , pImage : BitmapData = null, pImageURL:String = null) : void {
			//super.postOnFacebook(msg, pURL, pImage);
			launchFBUI (msg , pURL , pImage, pImageURL);
		}
		
		public function launchFBUI (msg : String , pUrl : String = null , pImage : BitmapData = null, pImageURL:String = null) : void {
			CONFIG::ios {
				sUI = new SocialUI (SocialServiceType.FACEBOOK);
				msgFlag = sUI.setMessage (msg);
				if (pUrl)
					urlFlag = sUI.addURL (pUrl);
				
				if (pImage)
					sUI.addImage (pImage);
				
				sUI.launch ();
			}
			CONFIG::android {
				postOnFacebook (msg, pUrl, pImageURL);
			}
		}
		
		public function launchTweetUI (msg : String , pUrl : String = null , pImage : BitmapData = null, pImageURL:String = null) : void {
			CONFIG::ios {
				sUI = new SocialUI (SocialServiceType.TWITTER);
				msgFlag = sUI.setMessage (msg);
				
				if (pUrl)
					urlFlag = sUI.addURL (pUrl);
				
				if (pImage)
					sUI.addImage (pImage);
				
				sUI.launch ();
			}
			CONFIG::android {
				postOnTwitter (msg, pUrl, pImageURL);
			}
		}
		
		protected function loadImageSocial (urlImage:String) : void {
			var loader : Loader = new Loader ();
			loader.load (new URLRequest (urlImage));
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE , onPicLoad)
		}
		
		protected function onPicLoad (e : Event) : void {
			var ld : LoaderInfo = LoaderInfo (e.target);
			trace (ld.toString ());
			bmd = new BitmapData (ld.width , ld.height , false , 0xFFFFFF);
			bmd.draw (ld.loader);
		}
	
	}

}
﻿package com.benoitfreslon {
	/**
	 * ...
	 * @author Benoît Freslon
	 * @version 1.31
	 */
	import com.*;
	import com.benoitfreslon.*;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.ContextMenuEvent;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.net.SharedObject;
	import flash.net.URLRequest;
	import flash.system.Security;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	
	public class MyMainWeb extends MyMainBase {
		
		///////////////////////////////////////////////////////////////////////
		// Set this variables
		public var localURL : String;
		public var isSitelocked : Boolean = false;
		public var arrDomains : Array;
		public var isKongregateTest : Boolean = false;
		
		///////////////////////////////////////////////////////////////////////
		
		static public var instance : MyMainWeb;
		
		///////////////////////////////////////////////////////////////////////
		
		public var domain : String;
		public var kongregate : * = null;
		
		protected var _isLocalMode : Boolean = false;
		protected var _isCookieLoaded : Boolean = true;
		protected var _cookieAttempts : int = 3;
		
		public function MyMainWeb () : void {
			super ();
			instance = this;
			Security.allowDomain ("*");
			Security.allowInsecureDomain ("*");
		}
		
		protected override function init (e : Event = null) : void {
			
			Security.allowDomain ("benoitfreslon.com");
			Security.allowDomain ("www.benoitfreslon.com");
			
			try {
				Security.loadPolicyFile ("http://www.benoitfreslon.com/crossdomain.xml");
			} catch (e : Error) {
				errorMessage ("Can't load crossdomain file.");
			}
			
			myContextMenu ();
			
			//stage.addEventListener(KeyboardEvent.KEY_UP, keyUp);
			//stage.scaleMode = StageScaleMode.NO_SCALE;
			
			var arr : Array = getLoaderInfo ().url.split ("://");
			domain = arr[1].split ("/")[0];
			trace ("domain:" + domain);
			testLocal ();
		}
		///////////////////////////////////////////////////////// LOOKIE
		override protected function loading (e : Event) : void {
			//trace(cookieLoaded,xmlLanguage != null,gameLoaded);
			if (_isCookieLoaded && _languageXML != null && _isGameLoaded) {
				allIsLoaded ();
				removeEventListener (Event.ENTER_FRAME , loading);
			}
		}
		///////////////////////////////////////////////////////// COOKIE
		public function getLoaderInfo () : LoaderInfo {
			var li : LoaderInfo = stage.loaderInfo;
			if (li.loader != null && li.loader.loaderInfo != null) {
				li = li.loader.loaderInfo;
			}
			return li;
		}
		
		protected function getGlobalCookie () : void {
			trace ("MyMain: getGlobalCookie" , cookieName);
			_isCookieLoaded = false;
			Security.loadPolicyFile ("http://www.benoitfreslon.com/crossdomain.xml");
			var mfl : MyFileLoader = new MyFileLoader ("http://www.benoitfreslon.com/modules/cookie/cookie_as3.swf" , cookieDownloaded , cookieError);
		}
		
		// Get global cookie
		private function cookieDownloaded (e : Event) : void {
			var objetLoaderInfo : LoaderInfo = LoaderInfo (e.target);
			var content : DisplayObject = objetLoaderInfo.content;
			addChild (content);
			cookieLoaded (content["getCookie"] (cookieName));
			_isCookieLoaded = true;
		}
		
		protected function cookieLoaded (obj : Object) : void {
			so = obj;
		}
		
		private function cookieError () : void {
			trace ("MyMain: cookieError" , _cookieAttempts);
			var err : Error;
			_cookieAttempts--
			if (_cookieAttempts > 0) {
				errorMessage ("Cookie error, retry " + _cookieAttempts);
				getGlobalCookie ();
				return;
			}
			trace ("Load local cookie");
			so = SharedObject.getLocal (cookieName , "/")
			if (so) {
				
				errorMessage ("Can't load global cookie, loading local cookies");
				cookieLoaded (so);
			} else {
				errorMessage ("Cookies disabled");
				cookieLoaded ({data: {} , flush: function () {
				}})
			}
			
			_isCookieLoaded = true;
		}
		///////////////////////////////////////////////////////// LOCAL
		private function testLocal () : void {
			//trace("testlocal",urlLocal);
			if (getLoaderInfo ().url == localURL) {
				////traceMy("Main: localmode true");
				_isLocalMode = true;
			}
		}
		///////////////////////////////////////////////////////// CONTEXT MENU
		protected function myContextMenu () : void {
			//trace("Main: myContextMenu");
			
			var cm : ContextMenu = new ContextMenu ();
			//cm.hideBuiltInItems();
			cm.builtInItems.forwardAndBack = false;
			cm.builtInItems.loop = false;
			cm.builtInItems.play = false;
			cm.builtInItems.print = false;
			cm.builtInItems.quality = true;
			cm.builtInItems.rewind = false;
			cm.builtInItems.save = false;
			cm.builtInItems.zoom = false;
			var cmi1 : ContextMenuItem = new ContextMenuItem (gameNameCopyrights);
			cmi1.addEventListener (ContextMenuEvent.MENU_ITEM_SELECT , clickWebsite);
			cmi1.addEventListener (ContextMenuEvent.MENU_ITEM_SELECT , clickWebsite);
			var cmi2 : ContextMenuItem = new ContextMenuItem ("Benoît Freslon");
			cmi2.addEventListener (ContextMenuEvent.MENU_ITEM_SELECT , clickWebsite);
			var cmi3 : ContextMenuItem = new ContextMenuItem ("www.benoitfreslon.com");
			cmi3.addEventListener (ContextMenuEvent.MENU_ITEM_SELECT , clickWebsite);
			cm.customItems = [cmi1 , cmi2 , cmi3];
			contextMenu = cm;
		}
		///////////////////////////////////////////////////////// KONGEGATE
		protected function kongregateAPI () : void {
			if (!isKongregateTest) {
				if (domain != "chat.kongregate.com" && domain != "kongregate.com") {
					return;
				}
			}
			
			trace ("Kongregate: API loaded")
			
			// Pull the API path from the FlashVars
			var paramObj : Object = LoaderInfo (root.loaderInfo).parameters;
			
			// The API path. The "shadow" API will load if testing locally. 
			var apiPath : String = paramObj.kongregate_api_path || "http://www.kongregate.com/flash/API_AS3_Local.swf";
			
			// Allow the API access to this SWF
			Security.allowDomain (apiPath);
			
			try {
				var request : URLRequest = new URLRequest (apiPath);
				var loader : Loader = new Loader ();
				loader.contentLoaderInfo.addEventListener (Event.COMPLETE , loadComplete);
				loader.load (request);
				this.addChild (loader);
			} catch (error : Error) {
				trace ("Kongregate: Can't load kongregate API");
			}
		}
		
		private function loadComplete (e : Event) : void {
			trace ("Kongregate: loadComplete");
			kongregate = e.target.content;
			Security.allowDomain (kongregate.loaderInfo.url);
			kongregate.services.connect ();
			//trace ("Kongregate:" + kongregate.services );
			//trace ("Kongregate:" + kongregate.user );
			//trace ("Kongregate:" + kongregate.scores );
			//trace ("Kongregate:" + kongregate.stats );
			activeAds = false;
			kongregateLoaded ();
		}
		protected function kongregateLoaded () : void {
		
		}
	}
}
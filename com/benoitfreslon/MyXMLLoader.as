﻿package com.benoitfreslon
{
	
	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.01
	 */
	
	 
    import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
    import flash.net.URLRequest;
	import flash.net.URLLoaderDataFormat;
	import flash.events.IEventDispatcher;
	import flash.events.HTTPStatusEvent;
	import flash.net.URLVariables;
	 
	public class  MyXMLLoader
	{
		/**
		 * Override this property with a function with one parameter with XML type.
		 * 
		 * @example function loaded(xml:XML):void {} 
		 */
		public var fcCallback:Function = null;

        public function MyXMLLoader() {
			super();
        }
		public function load(pDownloadURL:String, pVariables:URLVariables = null):void {
            var loader:URLLoader = new URLLoader();
            configureListeners(loader);
			var request:URLRequest = new URLRequest(pDownloadURL);
			
			if (pVariables != null) {
				loader.data = pVariables
			}
            try {
                loader.load(request);
            } catch (error:Error) {
                trace("*** MyXMLLoader: Unable to load:", pDownloadURL);
            }
		}
        private function configureListeners(dispatcher:IEventDispatcher):void {
            dispatcher.addEventListener(Event.COMPLETE, completeHandler);
            dispatcher.addEventListener(Event.OPEN, openHandler);
            dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
            dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
            dispatcher.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);
            dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
        }

        private function completeHandler(event:Event):void {
            var loader:URLLoader = URLLoader(event.target);
           // trace("completeHandler: " + loader.data);
			if (fcCallback is Function) {
				fcCallback(new XML(loader.data));
			}
        }

        private function openHandler(event:Event):void {
            trace("*** MyXMLLoader: openHandler: " + event);
        }

        private function progressHandler(event:ProgressEvent):void {
           trace("*** MyXMLLoader: progressHandler loaded:" + event.bytesLoaded + " total: " + event.bytesTotal);
        }

        private function securityErrorHandler(event:SecurityErrorEvent):void {
            trace("*** MyXMLLoader: securityErrorHandler: " + event);
        }

        private function httpStatusHandler(event:HTTPStatusEvent):void {
            trace("*** MyXMLLoader: httpStatusHandler: " + event);
        }

        private function ioErrorHandler(event:IOErrorEvent):void {
            trace("*** MyXMLLoader: ioErrorHandler: " + event);
        }

	}
}
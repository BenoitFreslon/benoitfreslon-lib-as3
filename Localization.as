﻿package
{
	
	/**
	 * ...
	 * @author Benoît Freslon
	 */
	import flash.utils.Dictionary;
	dynamic public class Localization extends Dictionary {
		
		static public function key(k:String, def:String = null):String {
			if (Localization[k] == null) {
				trace(new Error("Localization - No value found for key: " + k+" default value: "+def));
				if (def == null)
					return k;
				else
					return def;
			}
			return Localization[k];
		}
		
	}
	
}
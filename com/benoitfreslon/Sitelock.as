﻿
package com.benoitfreslon {

	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.11
	 */
	import flash.display.LoaderInfo;
	import flash.display.MovieClip
	import flash.display.StageScaleMode;
	import flash.events.MouseEvent;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.TextField;
	import flash.media.SoundMixer;

	public class Sitelock {

		public var mc:MovieClip
		public var newAddress:String;
		public var arrUrl:Array;
		public var isAut:Boolean;
		public var sitelock:Boolean = true;
		public var onLocal:Boolean = false;
		
		public var url:String;
		public var domain:String;

		public function Sitelock(pMc:MovieClip, pNewAddress:String, pArrUrl:Array, pLocal:Boolean = true, pSiteLock:Boolean = true ):void {
			trace("*** Sitelock: constructor v1.10", arrUrl);
			
			mc = pMc;
			newAddress = pNewAddress;
			arrUrl = pArrUrl;
			isAut = false;
			sitelock = pSiteLock;
			
			if (!sitelock) {
				return;
			}
			
			url = getMainLoaderInfo().url;
			var arr:Array = getMainLoaderInfo().url.split("://");
			
			domain = arr[1].split("/")[0];

			// localmode
			if (url.split("file://").length > 1) {
				onLocal = true;
				if (pLocal) {
					trace("Local mode is autorized");
					return
				} else {
					destroy("Local mode is restricted");
					return
				}
			}

			for (var i:String in arrUrl) {
				trace(domain.split(arrUrl[i]).length, arrUrl[i]);
				arr = domain.split(arrUrl[i])
				if (arr.length > 1) {
					isAut = true;
					domain = arrUrl[i];
					//trace("*** Sitelock: Domain found");
					break;
				}

			}

			if (!isAut) {
				//trace("*** Sitelock: Not authentified");
				destroy("Sorry, unauthaurized domain: "+domain+". Please contact me if you want this game on your website:\ncontact@benoitfreslon.com\n © http://www.benoitfreslon.com");

			}
			return;
		}
		private function getMainLoaderInfo():LoaderInfo {
			var loaderInfo:LoaderInfo = mc.loaderInfo;
			if (loaderInfo.loader != null) {
				loaderInfo = loaderInfo.loader.loaderInfo;
			}
			return loaderInfo;
		}
		/**
		 *
		 * @param	pMc
		 * @param	pNewAddress
		 * @param	pArrUrl
		 * @param	pLocal
		 */
		public static function check(pMc:MovieClip, pNewAddress:String, pArrUrl:Array, pLocal:Boolean = true, pSiteLock:Boolean = true ):Sitelock {
			return new Sitelock(pMc, pNewAddress, pArrUrl, pLocal, pSiteLock);
		}
		private function destroy(pError:String):void {
			if (!sitelock) {
				return;
			}
			trace("*** Sitelock: destroy",pError, arrUrl);

			SoundMixer.stopAll();
			SoundMixer.soundTransform = new SoundTransform(0, 0);

			var i = mc.numChildren;
			while (i--) {
				//mc.removeChildAt(0);
				mc.visible = false;
				mc.stop();
			}

			//mc.gotoAndStop(1);
			var black:MovieClip = new MovieClip();
			black.graphics.beginFill(0x000000);
			black.graphics.drawRect(0,0,10000,10000);
			black.graphics.endFill();
			mc.addChild(black);

			var txt:TextField = new TextField();
			txt.backgroundColor = 0x0000;
			txt.border = true;
			txt.background = true;
			txt.selectable = false;
			txt.textColor = 0xFFFFFF;
			txt.borderColor = 0xFFFFFF;
			txt.htmlText = "<p align='center'><a href='"+newAddress+"'><font face='Verdana'><b>Sorry!</b>\n\n<font color='#FF9900'><b>"+pError+"</b></font>\n\nPlease go to\n<u>"+newAddress+"\n\n<b>Click here to play the game</a></p>";
			//txt.autoSize = "center";
			//txt.buttonMode = true;
			//txt.width = mc.stage.stageWidth;
			//txt.height = mc.stage.stageHeight;
			txt.autoSize = "center"
			//trace(mc.stage.stageWidth);
			mc.stage.scaleMode = StageScaleMode.NO_BORDER;
			//txt.x = mc.stage.stageWidth / 2-txt.width/2;
			//txt.y = mc.stage.stageHeight / 2-txt.height/2;
			txt.x = 0;
			txt.y = 0;
			mc.buttonMode = true;
			mc.stage.addChild(txt);
			mc.stage.addEventListener(MouseEvent.CLICK, gotoWebsite )
		}
		private function gotoWebsite(pEvt:MouseEvent):void
		{
			var request:URLRequest = new URLRequest(newAddress);

			try {
				navigateToURL ( request, "_blank" );
			} catch (error:Error) {

			}

		}

	}
}


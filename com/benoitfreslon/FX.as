package com.benoitfreslon 
{
	import flash.display.GradientType;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.SpreadMethod;
	import flash.geom.Matrix;
	import flash.text.TextField;
	/**
	 * ...
	 * @author Benoît Freslon
	 */
	public class FX 
	{
		
		public function FX() 
		{

		}
		
		static public function textGradientColor(mc:MovieClip, arrCol:Array, t:TextField, angle:Number = Math.PI/2):void {
			var m:Shape = new Shape();
 			var fillType:String = GradientType.LINEAR;
			var colors:Array = arrCol;
			var alphas:Array = [1, 1];
			var ratios:Array = [0x00, 0xFF];
			var matr:Matrix = new Matrix();
			 
			matr.createGradientBox(10, 10, angle, 0, 0);
			//matr.createGradientBox()
			var spreadMethod:String = SpreadMethod.PAD;
			 
			m.graphics.beginGradientFill(fillType, colors, alphas, ratios, matr, spreadMethod);  
			m.graphics.drawRect(-t.width/2,-t.height/2,t.width,t.height);
			 
			m.mask = t;
			 
			mc.addChild(m)
		}
	}
}
﻿package com.benoitfreslon {

	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.10
	 */

	import com.greensock.easing.Linear;
	import com.greensock.TweenLite;
	import flash.media.Sound;
	import flash.media.SoundTransform;
	import flash.media.SoundChannel;
	import flash.media.SoundLoaderContext;
	import flash.net.URLRequest;
	import flash.events.Event;
	import flash.utils.setInterval;
	import flash.utils.clearInterval;
	import flash.media.SoundMixer;

	public class MySound extends Sound{

		public static var arrSound:Array = [];
		public static var arrSoundPaused:Array = [];
		public static var allPlaying:Boolean = true;
		
		static public var muted:Boolean = false;
		
		// Fading
		private var intervalFade:int = 0
		private var timesPlayed:int = 0;
		public var isPlaying:Boolean = false;

		public var sdChannel:SoundChannel = new SoundChannel();

		private var pausePoint:Number;
		private var loops:int = 0;
		private var sdTransform:SoundTransform = new SoundTransform();
		private var fadeSpeed:Number = 0
		public function MySound(stream:URLRequest=null,context:SoundLoaderContext=null):void {
			super(stream, context);
			arrSound.push(this);
		}

		static public function mute():void {
			muted = true;
			SoundMixer.soundTransform = new SoundTransform(0);
		}
		static public function unmute():void {
			muted = false;
			SoundMixer.soundTransform = new SoundTransform(1);
		}
		
		override public function play(pStartTime:Number = 0, pLoops:int = 0, pSdTransform:SoundTransform = null):SoundChannel {
			//trace("MySound: play ", timesPlayed++)
			loops = pLoops;
			if (pSdTransform == null) {
				//trace("*** MySound play",pan, volume);
				if (sdTransform == null) {
					//sdTransform = new SoundTransform();
				}
				pSdTransform = sdTransform;
				
			}
			//trace(pSdTransform, sdTransform, pStartTime, pLoops );
			if (sdChannel) {
				sdChannel.removeEventListener(Event.SOUND_COMPLETE, soundCompleteHandler);
			}
			/*
			// If sounds are muted
			if (muted) {
				pSdTransform.volume = 0;
			}
			*/
			sdChannel = super.play(pStartTime, pLoops, pSdTransform);
			if (sdChannel) {
				isPlaying = true;
				sdChannel.addEventListener(Event.SOUND_COMPLETE, soundCompleteHandler);
			}
			return sdChannel;
		}
		public function pause():void {
			//trace("MySound: pause", isPlaying);
			if (isPlaying) {
				pausePoint = sdChannel.position;
				sdChannel.stop();
				isPlaying = false;
				//remove();
			}
		}
		public function resume():void {
			if (!isPlaying) {
				play(pausePoint, loops, sdTransform);
				//sdChannel = play(pausePoint, loops, sdTransform);
				//sdChannel.addEventListener(Event.SOUND_COMPLETE, soundCompleteHandler);
				//isPlaying = true;
			}
		}
		public function stop():void {
			if (sdChannel) {
				sdChannel.stop();
			}
			isPlaying = false;
			remove();
		}

		/**
		 * Fade the sound.
		 *
		 * @param	pNextVolume Next volume (0 - 1)
		 * @param	pTime 		Time in s
		 * @param	pFunction	Callback function
		 */
		public function fade(pNextVolume:Number = 1, pTime:Number = 1, pFunction:Function = null):void {
			//TweenLite.to(this, pTime, {volume:pNextVolume, onUpdate:updateChannel, onComplete:onEndFade, onCompleteParams:[pFunction], ease:Linear.easeIn});
		}
		
		private function onEndFade(pCallback:Function):void 
		{
			stop();
			if (pCallback != null) {
				pCallback();
			}
		}
		private function updateChannel():void{
			  sdChannel.soundTransform = sdTransform;
		}
		
		public function set pan(pPan:Number):void {
			if (!sdChannel) {
				return;
			}
			sdTransform.pan = pPan;
			sdChannel.soundTransform = sdTransform
		}
		public function get pan():Number {
			//trace("*** MySound get pan",_pan);
			return sdTransform.pan
		}

		public function set volume(pVolume:Number):void {
			//trace("*** MySound set volume", pVolume);
			if (!sdChannel) {
				return;
			}
			//sdTransform.volume = pVolume*SoundMixer.soundTransform.volume;
			sdTransform.volume = pVolume;
			sdChannel.soundTransform = sdTransform
		}
		public function get volume():Number {
			//trace("*** MySound get volume",_volume);
			return sdTransform.volume
		}
		public function stopFade():void {
			//trace("*** MySound: stopFade");
			clearInterval(intervalFade);
		}
		public function autoPan(pX:Number, pStageWidth:int):void {
			pan = (pX-(pStageWidth / 2)) / (pStageWidth / 2);
		}
		private function soundCompleteHandler(pEvt:Event):void {
			//trace("MySound: soundCompleteHangler");
			isPlaying = false;
			remove();
		}
		public static function pauseAll():void {
			//trace("*** MySound: pauseAll", arrSound.length);
			allPlaying = false;
			for each (var t:MySound in arrSound) {
				//trace("sound isplaying ?",t.isPlaying)
				if (t.isPlaying) {
					t.pause();
					arrSoundPaused.push(t);
				}
			}
			
		}
		public static function resumeAll():void {
			//trace("*** MySound: resumeAll");
			allPlaying = true;
			for each (var t:MySound in arrSoundPaused) {
				t.resume();
			}
			arrSoundPaused = [];
			
		}
		public static function stopAll():void {
			for each (var t:MySound in arrSound) {
				t.stop();
			}
			arrSound = [];
		}
		public function remove():void {
			//trace("*** MySound: remove");
			var arr:Array = [];
			for each (var t:MySound in arrSound) {
				if (t != this) {
					arr.push(t);
				}
			}
			arrSound = arr;
		}
	}
}

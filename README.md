#SDK
[Adobe AIR SDK Download page](https://helpx.adobe.com/fr/air/kb/archived-air-sdk-version.html)

**Installation**

Help > Manage AIR SDK


# ERREURS

## Error: Comparison method violates its general contract!
Build halted with errors (fcsh).

Ouvrir C:\Program Files (x86)\FlashDevelop\Apps\flexairsdk\4.6.0+14.0.0\bin\jvm.config

java.home= "C:\Program Files (x86)\Java\jre6\"

# Arguments to VM

java.args=-Xmx1024m -Dsun.io.useCanonCaches=false

## iOS Export

https://helpx.adobe.com/flash/using/packaging-applications-air-ios.html

Kit SDK du simulateur iOS
`/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator.sdk`

Kit SDK iOS
`/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS.sdk`

Mode GPU
﻿package com.benoitfreslon
{

	/**
	 * ...
	 * @version 1.01
	 * @author Benoît Freslon
	 */

	import flash.net.URLVariables;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLLoaderDataFormat;
	import flash.system.Security;
	import flash.events.Event;
	import flash.events.IOErrorEvent;

	public class MyStatistics
	{
		public static var enabled:Boolean = true;
		
		static public function send(pGame:String, pVersion:int, pLevel:int, pTime:int, pStatus:int = 1):void {

			if (!enabled) {
				return;
			}
			
			Security.allowDomain("*");
			Security.allowInsecureDomain("*");
			Security.loadPolicyFile("http://www.thisisgameplay.com/crossdomain.xml");

			var variables:URLVariables = new URLVariables();
			variables.game = pGame;
			variables.version = pVersion;
			variables.level = pLevel;
			variables.time = pTime;
			variables.status = pStatus;

			var loader:URLLoader = new URLLoader();
			loader.dataFormat = URLLoaderDataFormat.VARIABLES;
			loader.addEventListener(Event.COMPLETE, handleComplete);
			loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			
			var request:URLRequest = new URLRequest("http://www.thisisgameplay.com/modules/stats/save_stats.php");
			request.method = URLRequestMethod.POST;
			request.data = variables;
			//trace(variables);
			//Debug.object(request);

			try {
				trace("Save stats: ", request);
				loader.load(request);
			} catch (error:Error) {
				trace("Unable to load requested document.");
			}
			
			
			function handleComplete(event:Event):void {
				//trace('ok');
				var loader:URLLoader = URLLoader(event.target);
				trace("Stats saved ? ",loader.data.success);
				//Debug.(loader.data);
				/*
				var loader:URLLoader = URLLoader(event.target);
				trace("Par: " + loader.data.par);
				trace("Message: " + loader.data.msg);
				/**/
			}
			function onIOError(event:IOErrorEvent):void {
				trace("Error loading URL.");
			}
		}

	}
}


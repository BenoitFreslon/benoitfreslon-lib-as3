﻿package com.benoitfreslon {
	
	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.12
	 */

	public class JaugeCircle extends MyMovieClip {
		
		private var _value:Number = 0;
		
		public function JaugeCircle() {
			stop();
		}
		public function set value(pValue:Number):void {
			_value = pValue;
			var frame:int = Math.abs(pValue*100);
			//trace("Frame " + frame);

			gotoAndStop(frame+1);
		}
		public function get value():Number {
			return _value;
		}
		public function reverse():void {
			scaleX = -scaleX;
			//scaleY = -scaleY;
		}
	}
}
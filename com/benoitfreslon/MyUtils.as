package com.benoitfreslon {
	import flash.utils.describeType;
	
	/**
	 * ...
	 * @author Benoît Freslon
	 */
	public class MyUtils {
		
		public function MyUtils() {
		
		}
		
		static public function isValidEmail( email : String ) : Boolean {
			var emailExpression : RegExp = /([a-z0-9._-]+?)@([a-z0-9.-]+)\.([a-z]{2,4})/;
			return emailExpression.test( email );
		}
		
		/**
		 * this method will work for retrieving properties of a *non-dynamic* (typed) object
		 * @return - all object properties
		 */
		
		static public function getProperties( _obj : * ) : Array {
			var _description : XML = describeType( _obj );
			var _properties : Array = new Array();
			for each ( var prop : XML in _description.accessor ) {
				var _property : Object = new Object();
				_property.name = String( prop.@name );
				_property.type = String( simple_type( prop.@type ) );
				_property.access = String( prop.@access );
				_property.declaredBy = String( prop.@declaredBy );
				try {
					_property.value = _obj[ _property.name ];
				} catch ( e : Error ) {
					_property.value = "";
				}
				_properties.push( _property )
			}
			_properties.sortOn( "name" );
			return _properties;
		}
		
		/**
		 * better format for object class information
		 * @param	_type
		 * @return
		 */
		private static function simple_type( _type : String ) : String {
			var lastIndex : int = _type.lastIndexOf( "::" );
			_type = lastIndex > 0 ? _type.substr( lastIndex + 2 ) : _type;
			return _type;
		}
		/*
		public static function getProperties( obj : * ) : String {
			var p : *;
			var res : String = '';
			var val : String;
			var prop : String;
			for ( p in obj ) {
				prop = String( p );
				if ( prop && prop !== '' && prop !== ' ' ) {
					val = String( obj[ p ] );
					if ( val.length > 10 )
						val = val.substr( 0, 10 ) + '...';
					res += prop + ':' + val + ', ';
				}
			}
			res = res.substr( 0, res.length - 2 );
			return res;
		}
		*/
		public static function getFrameLabelNumber(label:String, mc:MovieClip):uint {
			var labels:Array = mc.currentLabels;
			var i:int = labels.length;
			while (i--) {
				if (labels[i].name == label) {
					return labels[i].frame;
				}
			}
			return mc.currentFrame;
		}
	}

}
﻿package com.benoitfreslon {
	
	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.02
	 */
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class MyPoint extends Point {
		
		public var vector : Point = new Point();
		
		///////////////////////////////////////////////////////////////////////// GEOM
		public static function getBarycentre( pRec : Object ) : Point {
			return new Point( pRec.x + pRec.width / 2 , pRec.x + pRec.height / 2 );
		}
		
		public static function getAngleDeg( pPt1 : Object , pPt2 : Object ) : Number {
			return Math.atan2( pPt1.y - pPt2.y , pPt1.x - pPt2.x ) * MyMath.COEF_RAD_2_DEG;
		}
		
		public static function getAngleRad( pPt1 : Object , pPt2 : Object ) : Number {
			return Math.atan2( pPt1.y - pPt2.y , pPt1.x - pPt2.x );
		}
		
		public static function scalarProduct( pPt1 : Object , pPt2 : Object ) : Number {
			return pPt1.x * pPt2.x + pPt1.y * pPt2.y;
		}
		
		public static function vectorLenght( pV : Object ) : Number {
			return Math.sqrt( pV.x * pV.x + pV.y * pV.y );
		}
		
		public static function unitVector( pV : Object ) : Point {
			var len : Number = MyPoint.vectorLenght( pV );
			return new Point( pV.x / len , pV.y / len );
		}
		
		public static function lineToVector( pPt1 : Object , pPt2 : Object ) : Point {
			return new Point( pPt2.x - pPt1.x , pPt2.y - pPt1.y );
		}
		
		public static function projectVector( pV : Object , pVp : Object ) : Point {
			var uv : Point = MyPoint.unitVector( pVp );
			var sp : Number = MyPoint.scalarProduct( pV , uv );
			return new Point( uv.x * sp , uv.y * sp );
		}
		
		/*
		   public static function getPointTriangleRectangle(pt1:Point,pt2:Point,angle:Number):Point {
		
		   var ptC = Point.interpolate(pt1,pt2,0.5);
		
		   var distance:Number = Point.distance(pt1,pt2);
		
		   var angle = angle*Math.PI/180;
		   var newX:Number = pt1.x+Math.cos(angle)*(distance);
		   var newY:Number = pt1.y+Math.sin(angle)*(distance);
		   var pt1H:Point= new Point(newX,newY);
		
		   newX = pt2.x+Math.cos(angle)*(distance);
		   newY = pt2.y-Math.sin(angle)*(distance);
		   var pt2H:Point= new Point(newX,newY);
		
		   return getPointCollision(pt1,pt1H,pt2,pt2H);
		   }
		 */
		public static function collisionObjectScreen( pObject : Object , pRec : Rectangle ) : void {
			if ( pObject.y < pRec.top ) {
				pObject.y = pRec.top + pObject.height / 2;
			} else if ( pObject.y > pRec.bottom ) {
				pObject.y = pRec.bottom - pObject.height / 2;
			}
			if ( pObject.x < pRec.left ) {
				pObject.x = pRec.left + pObject.width / 2;
			} else if ( pObject.x > pRec.right ) {
				pObject.x = pRec.right - pObject.width / 2;
			}
		}
		
		public static function intersectLines( pa1 : Point , pb1 : Point , pa2 : Point , pb2 : Point ) : Point {
			var a1 : Number = ( pb1.y - pa1.y ) / ( pb1.x - pa1.x );
			var a2 : Number = ( pb2.y - pa2.y ) / ( pb2.x - pa2.x );
			var b1 : Number = pa1.y - ( a1 * pa1.x );
			var b2 : Number = pa2.y - ( a2 * pa2.x );
			
			if ( a1 == a2 ) {
				return new Point( 0 , 0 );
			}
			var ptX : Number = ( b2 - b1 ) / ( a1 - a2 );
			
			var ptY : Number = ( a1 * ptX + b1 );
			
			//trace(ptX,ptY);
			
			var newPt : Point = new Point( ptX , ptY );
			
			var intersec : Boolean;
			
			if ( pa1.x < ptX && ptX < pb1.x && pa2.x < ptX && ptX < pb2.x ) {
				intersec = true;
			} else {
				intersec = false;
			}
			return newPt;
		}
		
		public static function intersectCircleAABB( center : Point , radius : Number , rect : Rectangle ) : Point {
			
			var closestPoint : Point = center.clone();
			
			if ( center.x < rect.left ) {
				closestPoint.x = rect.left;
			} else if ( center.x > rect.right ) {
				closestPoint.x = rect.right;
			}
			if ( center.y < rect.top ) {
				closestPoint.y = rect.top;
			} else if ( center.y > rect.bottom ) {
				closestPoint.y = rect.bottom;
				
			}
			var diff : Point = closestPoint.subtract( center );
			if ( diff.x * diff.x + diff.y * diff.y > radius * radius ) {
				return null;
			}
			return closestPoint;
		
		}
	}
}

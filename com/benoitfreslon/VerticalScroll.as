﻿package com.benoitfreslon {
	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.0
	 */
	
	import flash.display.MovieClip;

	public class VerticalScroll extends MyMovieClip {

		
		public function VerticalScroll():void {
			trace("Main");
		}
		
		public function moveUp(pDistance:Number):void 
		{
			background1.y -= pDistance;
			if (background1.y < -background1.height) {
				background1.y = background2.y+background2.height;
			}
			background2.y += pDistance;
			if (background2.y < -background2.height) {
				background2.y = background2.y+background2.height;
			}
		}
		public function moveDown(pDistance:Number):void 
		{
			background1.y += pDistance;
			if (background1.y > background1.height) {
				background1.y = -background1.height;
			}
			background2.y += pDistance;
			if (background2.y > background2.height) {
				background2.y = -background2.height;;
			}
		}
	}
}
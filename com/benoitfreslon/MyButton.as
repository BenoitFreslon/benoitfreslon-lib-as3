﻿package com.benoitfreslon {
	
	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.13
	 */
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.Sound;
	import flash.text.TextField;

	public class MyButton extends MovieClip {
		public var sdRollOver:Sound = null;
		public var sdRelease:Sound= null;
		
		
		public var onClick:Function = null;
		public var onRollOver:Function = null;
		public var onRollOut:Function = null;
		public var onPress:Function = null;
		public var onRelease:Function = null;

		private var arrColor:Array = [];
		
		function MyButton() {
			super();
			addEventListener(Event.ADDED_TO_STAGE, added, false, 0 , true);
			addEventListener(Event.REMOVED_FROM_STAGE, removed, false, 0, true);
			addEventListener(MouseEvent.MOUSE_DOWN, press, false, 0, true);
			addEventListener(MouseEvent.CLICK, click, false, 0, true);
			addEventListener(MouseEvent.MOUSE_OVER, rollOver, false, 0, true);
			addEventListener(MouseEvent.MOUSE_OUT, rollOut, false, 0, true);
			buttonMode = true;
			mouseChildren = false;
			useHandCursor = true;
			hitArea = this;
		}
		protected function added(pEvt:Event):void {
			stop();
			//trace("MyButton: added:",this["txt"]);
			if (getChildByName("txt")) {
				this["txt"].selectable = false;
			}
			//x = Math.round(x); 
			//y = Math.round(y);
		}
		public function set text(pText:String):void {
			//trace("Set text");
			if (getChildByName("txt")) {
				this["txt"].text = String(pText);
			}
		}
		public function get text():String {
			if (getChildByName("txt")) {
				return String(this["txt"].text);
			}
			return "";
		}
		public function set textColor(pColor:Number):void {
			if (! isNaN(pColor) && getChildByName("txt")) {
				this["txt"].textColor = pColor;
			}
		}
		public function get textColor():Number {
			if (getChildByName("txt")) {
				return this["txt"].textColor;
			}
			return 0;
		}
		public function setColors(pColorNormal:uint,pColorRollOver:uint,pColorPressed:uint):void {
			arrColor = [pColorNormal, pColorRollOver, pColorPressed];
			textColor = arrColor[0];
		}
		protected function press(pEvt:MouseEvent):void {
			//trace("press");
			stage.addEventListener(MouseEvent.MOUSE_UP, releaseOutside);
			goto(3);
			if (onPress != null) {
				onPress(pEvt);
			}
		}
		protected function click(pEvt:MouseEvent=null):void {
			//trace("MyButton click", pEvt);
			//trace("Go to state 2");
			goto(2);
			//trace("click",onClick, pEvt.currentTarget);
			if (onClick != null) {
				onClick(pEvt);
			}
			if (sdRelease != null) {
				sdRelease.play();
			}
		}
		protected function goto(pState:int):void {
			gotoAndStop(pState);
			textColor = arrColor[pState - 1];
		}

		protected function rollOver(pEvt:MouseEvent):void {
			//trace("Rollover");
			if (pEvt.buttonDown) {
				dispatchEvent(new MouseEvent(MouseEvent.MOUSE_DOWN));
			} else {
				goto(2);
			}
			if (sdRollOver) {
				sdRollOver.play();
			}
			if (onRollOver != null) {
				onRollOver(pEvt);
			}
		}
		protected function rollOut(pEvt:MouseEvent):void {
			goto(1);
			if (onRollOut != null) {
				onRollOut(pEvt);
			}
		}
		protected function releaseOutside(pEvt:MouseEvent):void {
			//trace("MyButton: releaseOutside");
			if (onRelease != null)
				onRelease (pEvt);
			stage.removeEventListener(MouseEvent.MOUSE_UP, releaseOutside);
			if (pEvt.currentTarget != this && !hitArea.hitTestPoint(mouseX, mouseY)) {
				dispatchEvent(new MouseEvent(MouseEvent.MOUSE_OUT));
			}
		}
		protected function removed(pEvt:Event):void {
			//trace("removed");
			removeEventListener(Event.REMOVED_FROM_STAGE, removed);
			removeEventListener(Event.ADDED_TO_STAGE, added);
			removeEventListener(MouseEvent.MOUSE_DOWN, press);
			removeEventListener(MouseEvent.CLICK, click);
			removeEventListener(MouseEvent.MOUSE_OVER, rollOver);
			removeEventListener(MouseEvent.MOUSE_OUT, rollOut);
			stage.removeEventListener(MouseEvent.MOUSE_UP, releaseOutside);
		}

	}
}
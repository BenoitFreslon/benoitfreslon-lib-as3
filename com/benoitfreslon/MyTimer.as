package com.benoitfreslon
{
 
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.utils.getTimer;
 
	/**
	 * 
	 * @author Benoît Freslon
	 * @version 1.03
	 */
	public class MyTimer extends Timer {
 
		public static var arrTimer:Array = [];
		public static var arrTimerPaused:Array = [];
		public static var allRunning:Boolean = true;
		
		public var _delay:Number;
		public var _lastTime:Number;
		public var _thisTime:Number = 0;
		public var fcTimer:Function = function():void {};
		public var fcTimerComplete:Function = function():void {};
		public var arg:*;		
		/**
		 * 
		 * @param	pDelay
		 * @param	pRepeat
		 * @param	pFcTimer
		 * @param	pFcTimerComplete
		 * @param	...pArg
		 */
		public function MyTimer(pDelay:Number=1000, pRepeat:uint=0, pFcTimer:Function=null,pFcTimerComplete:Function=null, ...pArg:*):void {
			arg = pArg;
			//trace("arg : " + arg, typeof(arg));
			
			delay = pDelay;
			super(delay, pRepeat);
			if (pFcTimer != null) {fcTimer = pFcTimer;}
			if (pFcTimerComplete != null) {fcTimerComplete = pFcTimerComplete;}
			arrTimer.push(this);
			addEventListener(TimerEvent.TIMER, timer);
			addEventListener(TimerEvent.TIMER_COMPLETE, timerComplete);
		}
		override public function set delay(pDelay:Number):void {
			//trace("*** MyTimer: set delay:", pDelay);
			_delay = pDelay;
			super.delay = pDelay;
		}
		override public function get delay():Number {
			return _delay;
		}
		override public function start():void {
			//trace("*** MyTimer: start", delay, super.delay);
			reset();
			_lastTime = getTimer();
			super.start();
			if (allRunning == false) {
				//trace("*** MyTimer: is paused !!!!!!")
				pause();
			}
		}
		public function startTimer(pDelay:Number):void {
			//trace("*** MyTimer: startTimer", pDelay);
			delay = pDelay;
			start();
		}
		public function setArg(...pArg):void {
			arg = pArg;
		}
		private function timer(pEvt:TimerEvent):void {
			//trace("*** MyTimer: timer", arg,delay,super.delay);
			fcTimer.apply(null, arg);			
			if (super.delay != delay) {
				super.delay = delay;
			}
		}
		private function timerComplete(pEvt:TimerEvent):void {
			//trace("*** MyTimer: ============================= timerComplete");
			fcTimerComplete.apply(null, arg);
		}
		public static function pauseAll():void {
			//trace("*** MyTimer: pauseAll");
			allRunning = false;
			for each (var t:MyTimer in arrTimer) {
				if (t.running) {
					t.pause();
					arrTimerPaused.push(t);
				}
			}
			
		}
		public static function resumeAll():void {
			//trace("*** MyTimer: resumeAll");
			allRunning = true;
			for each (var t:MyTimer in arrTimerPaused) {
				t.resume();
			}
			arrTimerPaused = [];
			
		}
		public static function stopAll():void {
			for each (var t:MyTimer in arrTimer) {
				t.stop();
			}
		}
		public static function killAll():void {
			//trace("*** MyTimer: removeAll");
			for each (var t:MyTimer in arrTimer) {
				t.remove();
			}
			arrTimer = [];
		}
		public function pause():void {
			//trace("*** MyTimer: pause");
			stop();
			_thisTime = getTimer() - _lastTime;
			if (allRunning == false) {
				arrTimerPaused.push(this);
			}
		}
 
		public function repeat(new_delay:Number):void {
			_lastTime = getTimer();
			delay = new_delay;
		}
 
		public function resume():void {
			//trace("*** MyTimer: resume");
			if (_thisTime > super.delay) {
				_thisTime = super.delay;
			}
			super.delay = super.delay - _thisTime;
			_lastTime = getTimer();
			super.start();
			_thisTime = 0;
		}
		public function remove():void {
			//trace("*** MyTimer: remove");
			stop();
			removeEventListener(TimerEvent.TIMER, timer);
			removeEventListener(TimerEvent.TIMER_COMPLETE, timerComplete);
			var arr:Array = [];
			for each (var t:MyTimer in arrTimer) {
				if (t != this) {
					arr.push(t);
				}
			}
			arrTimer = arr;
		}
	}
}
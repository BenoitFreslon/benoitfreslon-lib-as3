﻿package com.benoitfreslon.fx
{
	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.11
	 */
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.Event;
	
	public class Particles extends Sprite  
	{
		public var particle:Class;
		public var gravity:Number = 0.1;
		public var rotationSpeed:Number = 0;
		public var velocityMax:Number = 10
		public var slowDown:Number = 1;
		public var fadeOut:Number = 1; 
		public var frequency:uint = 1;
		public var lifeTime:uint = 1000;
		public var randomize:Number = 0.5;
		public var direction:Number = 0;
		public var angle:Number = 360;
		public var totalTime:uint = 0;
		public var nbPerFrame:uint = 1;
		
		public var activated:Boolean = false;
		static public var paused:Boolean = false;
		static public var speed:Number = 1;
		
		
		private var time:Number = 0;
		
		public function Particles (pParticle:Class = null):void {
			particle = pParticle;
			addEventListener(Event.REMOVED_FROM_STAGE, removed);
		}
		/**
		 * 
		 * @param	pParticle Class
		 */
		public function init(pParticle:Class):void {
			//trace("*** Particles : init", pParticle);
			particle = pParticle;
		}
		
		public function start():void {
			//trace("*** Particles : start");
			if (MovieClip(parent) == null) {
				return;
			}
			addEventListener(Event.ENTER_FRAME, enterFrame)
			activated = true;
			reset()
		}
		private function enterFrame(pEvt:Event):void {
			//trace("*** Particles : enterFrame");
			if (paused) {
				return;
			}
			
			if (time % frequency == 0) {
				for (var i:int = 0; i < nbPerFrame;i++) {
					var p:* = new particle();
					p.addEventListener(Event.ENTER_FRAME, moveParticle);
					p.addEventListener(Event.REMOVED_FROM_STAGE, removedParticle);
					p.velocity = velocityMax * (1 - randomize) + Math.random() * velocityMax * randomize;
					p.time = 0;
					
					var newAngle:Number = direction - angle/2 + Math.random() * angle;
					
					p.vectorX = Math.cos(newAngle * Math.PI / 180);
					p.vectorY = Math.sin(newAngle * Math.PI / 180);
					
					p.rotationSpeed = Math.random() * rotationSpeed;
					
					if (rotationSpeed > 0 ) {
						p.rotation = Math.random() * 360;
					}
					
					addChild(p);
				}
			}
			
			time += 1*Particles.speed;
			if (time >= totalTime / 1000 * stage.frameRate && totalTime != 0) {
				//trace("*** Particles : Stop enterFrame");
				removeEventListener(Event.ENTER_FRAME, enterFrame);
				
			}
		}
		
		private function moveParticle(pEvt:Event):void {
			if (paused) {
				return;
			}
			var p:* = pEvt.currentTarget;
			p.x += p.velocity * p.vectorX * Particles.speed;
			p.y += p.velocity * p.vectorY * Particles.speed;;
			
			p.alpha *= fadeOut*Particles.speed;;
			p.velocity *= slowDown*Particles.speed;;
			
			if (p.time >= lifeTime/1000*stage.frameRate) {
				removeChild(p);
			}
			
								
			p.vectorY += gravity*Particles.speed;;
			
			p.time += 1 * Particles.speed;
			
			p.rotation += rotationSpeed*Particles.speed;;
			
		}
		private function removedParticle(pEvt:Event) :void {
			//trace("Particle: removedParticle");
			pEvt.currentTarget.removeEventListener(Event.ENTER_FRAME, moveParticle)
			pEvt.currentTarget.removeEventListener(Event.REMOVED_FROM_STAGE, removedParticle)
		}
		
		public function stop():void {
			activated = false;
			removeEventListener(Event.ENTER_FRAME, enterFrame)
		}
		
		public function reset():void {
			while (numChildren) {
				removeChildAt(0);
			}
			time = 0;
			activated = false;
		}
		
		private function removed(pEvt:Event):void {
			removeEventListener(Event.ENTER_FRAME, enterFrame)
			removeEventListener(Event.REMOVED_FROM_STAGE, removed);
		}
	}
	
}
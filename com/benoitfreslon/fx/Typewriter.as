﻿package com.benoitfreslon.fx
{
	import flash.text.TextField;
	import flash.utils.setInterval;
	import flash.utils.clearInterval;
	/**
	 * 
	 * @version 1.12
	 * @author Benoît Freslon
	 */
	public class Typewriter
	{

		public var text:String = "";
		protected var tempText:String = "";
		public var delay:int = 20;
		protected var intervalTypewriter:uint = 0;
		public var textField:TextField;
		public var completed:Boolean = false;
		public var started:Boolean = false;

		protected var i:int = 0;

		public function Typewriter(ptextField:TextField, pText:String,pDelay:int =20):void {
			textField = ptextField;
			text = pText;
			delay = pDelay;
		}
		public function reset():void {
			setText ("");
		}
		public function start():void {
			//trace("*** Typewritter: start",textField.text)
			stop ();
			tempText = getText();
			i = 0;
			intervalTypewriter = setInterval(write, delay );
			write ();
			completed = false;
			started = true;
		}
		public function complete ():void {
			stop ( );
			setText (text);
			completed = true;
		}
		protected function write ():void {
			//trace("*** Typewritter: write", textField);
			if (text.charAt(i) == "<") {
				while (text.charAt(i) != ">" && i < text.length) {
					tempText += text.charAt(i);
					//textField.appendText(text.charAt(i));
					//textField.htmlText = textField.text;
					i++;
				}
			}
			//textField.appendText(text.charAt(i));
			tempText += text.charAt(i);
			setText (tempText);
			i++;
			//trace("*** Typewritter: write", textField.htmlText);
			if (text.length == i) {
				stop ();
				completed = true;
			}
		}
		protected function setText(t:String):void {
			textField.htmlText = t;
		}
		protected function getText ():String {
			return textField.text;
		}
		public function stop ():void {
			started = false;
			clearInterval(intervalTypewriter);
		}
	}

}

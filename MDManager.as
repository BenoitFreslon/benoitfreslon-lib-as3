package {
	import com.demonsters.debugger.MonsterDebugger;
	
	/**
	 * ...
	 * @author Benoît Freslon
	 */
	public class MDManager {
		
		public static function init( base:Object, address:String = "127.0.0.1" ):void {
			
			MonsterDebugger.initialize( base, address );
			MonsterDebugger.logger = function( ... rest ):void {
				if ( !MonsterDebugger.enabled ) {
					return;
				}
				var error:Error = new Error();
				var tr:String = "";
				
				for each ( var msg:String in rest ) {
					tr += msg + " ";
				}
				var s:String = error.getStackTrace();
				var arr:Array = s.split( "at " )[ 2 ].split( "()" );
				var stack:String = arr[ 0 ];
				var arrClass:Array = stack.split( "/" )[ 0 ].split( "::" );
				var className:String = arrClass[ arrClass.length - 1 ];
				var functionName:String = stack.split( "/" )[ 1 ];
				
				if ( functionName == null ) {
					functionName = className;
				}
				var arrLine:Array = arr[ 1 ].split( ":" );
				
				if ( arrLine.length > 1 ) {
					var line:String = arrLine[ arrLine.length - 1 ].split( "]" )[ 0 ];
					trace( "" + className + ":" + functionName + ":" + "" + line + " |", tr );
						//trace( "[" + className + "][" + functionName + "]" + "[" + line + "]", tr );
				} else {
					trace( "" + className + ":" + functionName + " |", tr );
						//trace( "[" + className + "][" + functionName + "]", tr );
				}
				var color:int = 0;
				
				for ( var i:int = 0; i < className.length; i++ ) {
					//trace( className.charCodeAt( i ) );
					color += className.charCodeAt( i ) * 1500;
				}
				MonsterDebugger.trace( className, tr, functionName, line, color );
			}
		
		}
	}
}
﻿package com.benoitfreslon {
	
	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.05
	 */
	
	import flash.display.MovieClip;
	import flash.display.Sprite
	import flash.display.DisplayObject;
	import flash.geom.ColorTransform;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class MyMovieClip extends MovieClip {
		
		private var _scale:Number = 1;
		private var ct:ColorTransform = new ColorTransform();
		
		public function MyMovieClip() {
			super();
		}
		/**
		 * 
		 * @param	pMc1
		 * @param	pMc2
		 * @return 	distance
		 */
		public function distanceWith(pMc:*):Number {
			return Math.sqrt(Math.pow(x - pMc.x, 2) + Math.pow(y - pMc.y, 2));
		}
		public function distanceWithMc(pMc:MovieClip):Number {
			return Math.sqrt(Math.pow(x - pMc.x, 2) + Math.pow(y - pMc.y, 2));
		}
		public function distancePointWith(pPoint:Point):Number {
			return Math.sqrt(Math.pow(x - pPoint.x, 2) + Math.pow(y - pPoint.y, 2));
		}
		public function angleRadianWith(pMc:MovieClip):Number {
			//return Math.sqrt(Math.pow(x - pMc.x, 2) + Math.pow(y - pMc.y, 2));
			return Math.atan2(pMc.y - y, pMc.x-x);
		}
		public function angleRadianPointWith(pPoint:Point):Number {
			//return Math.sqrt(Math.pow(x - pMc.x, 2) + Math.pow(y - pMc.y, 2));
			return Math.atan2(pPoint.y - y, pPoint.x-x);
		}
		/**
		 * 
		 * @param	pFrame
		 * @param	pSequence
		 */
		public function gotoAndStop2(pFrame:*, pSequence:String = null):void {
			addEventListener(Event.ENTER_FRAME, myGotoAndStop);
			function myGotoAndStop(pEvt:Event):void {
				removeEventListener(Event.ENTER_FRAME, myGotoAndStop);
				if (pSequence) {
					gotoAndStop(pFrame,pSequence);
				} else {
					gotoAndStop(pFrame);
					//trace("Goto and stop ", pFrame);
				}
			}
		}
		public function gotoAndPlay2(pFrame:*, pSequence:String = null):void {
			addEventListener(Event.ENTER_FRAME, myGotoAndPlay);
			function myGotoAndPlay(pEvt:Event):void {
				removeEventListener(Event.ENTER_FRAME,myGotoAndPlay);
				if (pSequence) {
					gotoAndPlay(pFrame,pSequence);
				} else {
					gotoAndPlay(pFrame);
				}
			}
		}

		public function mimic(pMc:MovieClip):void {
			gotoAndStop(pMc.currentFrame);
			alpha = pMc.alpha;
			scaleX = pMc.scaleX;
			scaleY = pMc.scaleY;
			width = pMc.width;
			height = pMc.height;
			rotation = pMc.rotation;
		}
		public function drawDebug(rect:Rectangle):void {
			graphics.clear();
			graphics.beginFill(0xFF0000, 0.5); // choosing the colour for the fill, here it is red
			graphics.drawRect(rect.x, rect.y, rect.width,rect.height); // (x spacing, y spacing, width, height)
			graphics.endFill(); // not always needed but I like to put it in to end the fill
		}
		public function set scale(pScale:Number):void {
			scaleX = pScale;
			scaleY = pScale;
			_scale = pScale;
		}
		public function get scale():Number {
			return _scale;
		}
		
		public function get color():int 
		{
			return ct.color
		}
		
		public function set color(value:int):void 
		{
			ct.color = value;
			transform.colorTransform = ct;
		}
		/**
		 * 
		 * @param
		 */
		public function removeAllChildren():void {
			while (numChildren) {
				removeChildAt(0);
			}
		}
		/**
		 * 
		 * @param
		 */
		public function remove():void {
			stop();
			if (Sprite(parent)) {
				if (Sprite(parent).contains(this)) {
					Sprite(parent).removeChild(this);
				}
			}
		}
	}
}
﻿package com.benoitfreslon {
	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.04
	 */
	import flash.events.Event;
	import flash.display.MovieClip;
	import flash.display.Stage;
	
	public class FrameTimer {
		
		static public var stage:Stage;
		
		private var framerate:int = 25;
		private var totalFrame:int = 0;
		
		public var fc:Function;
		public var param:*;
		public var t:int = 0;
		public var mc:MovieClip;
		private var _time:uint = 0;
		
		public static var paused:Boolean = false;
		
		public function FrameTimer(pMc:MovieClip, pFc:Function, pTime:int, pParam:* = null) {
			//trace("FrameTimer: ", pMc, pFc, pTime, pParam);
			mc = pMc;
			//trace( "pMc.stage : " + pMc.stage );
			framerate = pMc.stage.frameRate;
			//trace( "framerate : " + framerate );
			
			fc = pFc;
			param = pParam;	
			time = pTime;
			
			//pMc.addChild(this);
		}
		public function set time(pTime:int):void {
			totalFrame = totalFrame = pTime / 1000 * framerate;
			_time = pTime;
			//trace(mc,totalFrame);
		}
		public function get time():int {
			return _time;
		}
		public function start():void {
			//trace("*** FrameTimer: Timer started by ",mc.name,"===");
			t = 0;
			mc.addEventListener(Event.ENTER_FRAME, frame);
			//trace(mc, frame);
		}
		private function frame(pEvt:Event):void {
			//trace("*** FrameTimer: Timer frame by ",mc.name,t,"===");
			if (FrameTimer.paused) {
				return
			}
			t++;
			//trace(t,mc);
			if (t >= totalFrame) {
				//trace("Timer === Timer is over ===",mc.name);
				end();
			}
		}
		public function end():void {
			//trace("*** FrameTimer: end Timer",t,totalFrame,framerate);
			if (param) {
				fc(param);
			} else {
				fc();
			}
			stop();
		}
		public function reset():void {
			//trace("*** FrameTimer: reset");
			t = 0;
		}
		public function stop():void {
			//trace("*** FrameTimer: Timer has been stoped ===",mc.name);
			mc.removeEventListener(Event.ENTER_FRAME, frame);
		}		
	}
}
﻿package com.benoitfreslon {
	
	import flash.display.Sprite;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.net.*;
	import flash.system.Security;

	/**
	 * @author Benoît Freslon
	 * @version 0.1
	 */
	public class MyYoutube extends Sprite {

		
		public var player:Object;

		private var loader:Loader = new Loader();

		private var w:int = 0;
		private var h:int = 0;
		
		private var url:String = "";
		
		private var ready:Boolean = false;
		
		public function MyYoutube():void {
			trace("MyYoutube: constructor");
			// The player SWF file on www.youtube.com needs to communicate with your host
			// SWF file. Your code must call Security.allowDomain() to allow this
			// communication.
			Security.allowDomain("www.youtube.com");
			addEventListener(Event.REMOVED_FROM_STAGE, removed);
			loader.load(new URLRequest("http://www.youtube.com/apiplayer?version=3"));
			loader.contentLoaderInfo.addEventListener(Event.INIT, onLoaderInit);
		}

		public function load(pUrl:String, pWidth:int, pHeight:int):void {
			trace("MyYoutube: load", pUrl, pWidth, pHeight);
			//trace(ready, player);
			w = pWidth;
			h = pHeight
			url = pUrl;
			if (ready) {
				trace("Load video", pUrl);
				player.setSize(w, h);
				player.loadVideoByUrl(pUrl);
			}
		}
		
		public function pause():void {
			if (player) {
				player.pauseVideo();
			}
		}
		public function remove():void {
			removeEventListener(Event.REMOVED_FROM_STAGE, removed);
			pause();
			stop();
			if (loader) {
				if (contains(loader)) {
					removeChild(loader);
				}
				loader.unload();
				
			}
			if (loader.content) {
				loader.content.removeEventListener("onReady", onPlayerReady);
				loader.content.removeEventListener("onError", onPlayerError);
				loader.content.removeEventListener("onStateChange", onPlayerStateChange);
				loader.content.removeEventListener("onPlaybackQualityChange", onVideoPlaybackQualityChange);
			}
			loader = null;
		}
		public function stop():void {
			if (player) {
				trace("MyYoutube: stopVideo");
				player.stopVideo();
			}
		}
		private function removed(pEvt:Event):void {
			remove();
		}
		function onLoaderInit(event:Event):void {
			// This will hold the API player instance once it is initialized.
			addChild(loader);
			loader.content.addEventListener("onReady", onPlayerReady);
			loader.content.addEventListener("onError", onPlayerError);
			loader.content.addEventListener("onStateChange", onPlayerStateChange);
			loader.content.addEventListener("onPlaybackQualityChange", onVideoPlaybackQualityChange);
			
			
			
		}

		function onPlayerReady(event:Event):void {
			// Event.data contains the event parameter, which is the Player API ID
			trace("Youtube: onPlayerReady:", Object(event).data);

			// Once this event has been dispatched by the player, we can use
			// cueVideoById, loadVideoById, cueVideoByUrl and loadVideoByUrl
			// to load a particular YouTube video.
			player = loader.content;
			// Set appropriate player dimensions for your application
			ready = true;
			load(url,w,h);
		}

		function onPlayerError(event:Event):void {
			// Event.data contains the event parameter, which is the error code
			trace("player error:", Object(event).data);
		}

		function onPlayerStateChange(event:Event):void {
			// Event.data contains the event parameter, which is the new player state
			trace("player state:", Object(event).data);
		}

		function onVideoPlaybackQualityChange(event:Event):void {
			// Event.data contains the event parameter, which is the new video quality
			trace("video quality:", Object(event).data);
		}

	}
}

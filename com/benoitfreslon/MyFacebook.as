package com.benoitfreslon
{
	import com.adobe.serialization.json.JSON;
	import com.facebook.graph.Facebook;
	import com.greensock.TweenMax;
	import flash.display.GradientType;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.SpreadMethod;
	import flash.display.Sprite;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.external.ExternalInterface;
	import flash.geom.Matrix;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;
	import flash.net.navigateToURL;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.system.Capabilities;
	import flash.system.LoaderContext;
	import flash.system.Security;
	import flash.text.TextField;
	import flash.utils.*;
	

	/**
	 * @version 1.07
	 * @author Benoît Freslon
	 */
	
	public class MyFacebook
	{
		[Embed(source='/com/benoitfreslon/facebook_language.xml',mimeType="application/octet-stream")]
		static private const xmlLanguageBackup:Class;
		
		private const URL_LANGUAGE:String = "http://www.benoitfreslon.com/modules/facebook/language.xml";
		private var languageLoaded:Boolean = false;
		
		private var appID:String;
		
		public var userName:String = "user";
		public var uid:String = "";
		
		public var appUrl:String = "";
		public var appIcon:String = "";
		public var appName:String = "";
		public var appDescription:String = "";
		//public var appCaption:String = "";
		
		private var oFacebookSessionUser:Object;
		private var opts:Object;
		private var connected:Boolean = false;
		public var isOnFacebook:Boolean = true;
		public var arrFriendList:Array = [];
		
		private var userBestScore:int = 0;
		private var mc:Sprite;
		private var xmlLanguage:XML;
		private var language:String = "en";
		private var arrLanguage:Array = [];
		
		private var languageLoader:MyTextLoader = new MyTextLoader();
		
		private var cbConnected:Function;
		private var cbGetFriends:Function
		private var cbGetMyBestScore:Function;
		
		private var hud:MovieClip;
		
		
		
		/**
		 * Custom Facebook handler
		 * The swf should get a flashvar named: "appid"
		 *
		 *
		 * @param	pAppID
		 * @param	pOpts 	Exemple: {scope:"publish_stream, user_photos, user_birthday, read_stream"}
		 */
		public function MyFacebook(pMc:Sprite, pAppID:String, callback:Function = null ,pOpts:Object=null)
		{
			trace("*** MyFacebook: constructor", pAppID, pOpts);
			var loaderContext:LoaderContext = new LoaderContext (true);
			Security.allowDomain("*");
			Security.allowInsecureDomain("*");
			Security.loadPolicyFile("https://ssl14.ovh.net/~thisisga/crossdomain.xml");
			Security.loadPolicyFile("https://s-static.ak.facebook.com/crossdomain.xml");
			Security.loadPolicyFile("http://static.ak.connect.facebook.com/crossdomain.xml");
			Security.loadPolicyFile("https://www.facebook.com/crossdomain.xml");
			
			
			mc = pMc;
			
			appID = pAppID;
			cbConnected = callback;
			
			if (pOpts == null) {
				opts = { scope:"publish_stream, user_photos" };
			} else {
				opts = pOpts;
			}
			
			appDescription = getMainLoaderInfo().parameters.appDescription;
			appIcon = getMainLoaderInfo().parameters.appIcon;
			appUrl = getMainLoaderInfo().parameters.appUrl;
			appName = getMainLoaderInfo().parameters.appName;
			userBestScore = int(getMainLoaderInfo().parameters.bestScore);
			//appCaption = getMainLoaderInfo().parameters.appCaption;
			userName = getMainLoaderInfo().parameters.userName;
			uid = getMainLoaderInfo().parameters.uid;
			
			trace("parameters", appDescription, appIcon, appUrl, appName, getMainLoaderInfo().parameters.appID);
			
			testIsOnFacebook();
			
			if (isOnFacebook)
			{
				
				trace("*** MyFacebook: init Facebook");
				
				//connected = true;
				Facebook.init(appID, handleInit, opts); // Initialize Facebook library
				loadLanguage();
				//login();
			}
			//trace("show loaderinfo");
			//Debug.object(mc.loaderInfo.parameters);
		}
		private function getMainLoaderInfo():LoaderInfo {
			var loaderInfo:LoaderInfo = mc.loaderInfo;
			if (loaderInfo.loader != null) {
				loaderInfo = loaderInfo.loader.loaderInfo;
			}
			return loaderInfo;
		}
		private function testIsOnFacebook():void
		{
			var arr:Array = getMainLoaderInfo().url.split("://");
			var domain:String = arr[1].split("/")[0];
			//trace( "domain : " + domain );
			// localmode
			
			if (!ExternalInterface.available) {
				trace("*** MyFacebook: no ExternalInterface.available");
				isOnFacebook = false;
			} else if (getMainLoaderInfo().url.split("file://").length > 1)
			{
				isOnFacebook = false;
				trace("*** MyFacebook: not on facebook, local mode");
				
			} else if (getMainLoaderInfo().parameters.appID != appID)
			{
				isOnFacebook = false;
				trace("*** MyFacebook: not on my facebook page");
			}
		}
		protected function login():void {
			Facebook.login(onLogin, opts);
		}
		
		protected function onLogin(result:Object, fail:Object):void {
			if (result) { //successfully logged in
				trace("*** MyFacebook: loged in" , JSON.encode(result))
			} else {
				trace("*** MyFacebook: Failed to login", JSON.encode(fail));			
			}
		}
		private function handleInit(oFacebookSession:Object, fail:Object):void
		{
			
			// sauvegarde des données utilisateur
			oFacebookSessionUser = oFacebookSession;
			
			/**
			 * Données utilisateur disponibles :
			 * accessToken
			 * secret
			 * sessionKey
			 * sig
			 * uid
			 *
			 */
			
			if (oFacebookSessionUser)
			{
				// Utilisateur connecté à Facebook
				trace("*** MyFacebook: connected to Facebook");
				connected = true;
				Debug.object(oFacebookSessionUser);
				if (cbConnected != null) {
					cbConnected();
				}
			}
			else
			{
				trace("*** MyFacebook: not connected to Facebook");
				connected = false;
					// redirection vers la page de connection
			}
		}
		
		/*****************************************************************************
		 * Call UI
		 *
		 * @param	uid		The Facebook uid of the target. "me" : current user.
		 * @param	data
		 */
		public function callUI(method:String, data:Object = null):void
		{
			if (!connected || !isOnFacebook)
			{
				return;
			}
			trace("*** MyFacebook: callUI", method);
			if (data == null)
			{
				data = {};
			}
			Facebook.ui(method, data, onCallUI);
		}
		
		protected function onCallUI(result:Object):void
		{
			trace("*** MyFacebook: onCallUI: " + JSON.encode(result));
			//trace("*** MyFacebook: onCallUI: ");
		}
		
		/******************************************************************************
		 * Call API
		 *
		 * @param	uid		The Facebook uid of the target + the method. "/me/feed" : post feed on current users wall.
		 * @param	data
		 */
		public function callAPI(method:String, data:Object = null, requestType:String = "POST"):void
		{
			if (!connected || !isOnFacebook)
			{
				return;
			}
			trace("*** MyFacebook: callAPI", method);
			Debug.object(data);
			Facebook.api(method, onCallApi, data, requestType);
		}
		
		protected function onCallApi(result:Object, fail:Object):void
		{
			if (result)
			{
				trace("*** MyFacebook: onCallApi RESULT:" + JSON.encode(result));
					//trace("*** MyFacebook: onCallApi RESULT:"); 
			}
			else
			{
				trace("*** MyFacebook: onCallApi FAIL :" + JSON.encode(fail));
					//trace("*** MyFacebook: onCallApi FAIL :"); 
			}
		}
		
		/*******************************************************************************
		 * Save score
		 *
		 * @param	pScore
		 */
		public function saveScore(pScore:int):void
		{
			trace("*** MyFacebook: saveScore", connected, isOnFacebook,pScore, userBestScore);
			if (!connected || !isOnFacebook)
			{
				return;
			}
			
			
			
			if (pScore > userBestScore)
			{
				trace("*** MyFacebook: changeScore", pScore);
				ExternalInterface.call("changeScore", pScore);
				
				userBestScore = pScore;
				var variables:URLVariables = new URLVariables();
				variables.uid = oFacebookSessionUser.uid;
				variables.score = userBestScore;
				variables.game = appID;
				var save:MyTextLoader = new MyTextLoader("http://www.benoitfreslon.com/modules/facebook/save_score.php", variables, onSaveScoreComplete, onSaveScoreError);
			}
			compareScore();
		}
		
		private function onSaveScoreComplete(event:Event):void
		{
			var loader:URLLoader = URLLoader(event.target);
			trace("onSaveScoreComplete: " + loader.data);
		}
		
		private function onSaveScoreError():void
		{
			trace("onSaveScoreError: ");
		}
		/**
		 * 
		 * 
		 */
		
		public function postOnUserWall(uid:String, pMessage:String):void {
			trace("*** MyFacebook: postOnUserWall");
			var obj:Object = new Object();
			obj.message = pMessage
			obj.caption = appDescription;
			obj.description = appDescription;
			obj.name = appName;
			obj.link = appUrl;
			obj.picture = appIcon;
			//trace("click 1 UI");
			Debug.object(obj);
		
			callAPI(uid+"/feed", obj);
		}
		/*******************************************************************************
		 * Get the best score of the current player
		 *
		 * @param	pCBGetMyBestScore Callback functio with int parameter
		 */
		
		public function getMyBestScore(pCBGetMyBestScore:Function):void
		{
			if (!connected || !isOnFacebook)
			{
				return;
			}
			trace("*** MyFacebook: getMyBestScore");
			
			cbGetMyBestScore = pCBGetMyBestScore;
			
			var variables:URLVariables = new URLVariables();
			variables.uid = oFacebookSessionUser.uid;
			var save:MyTextLoader = new MyTextLoader("http://www.benoitfreslon.com/modules/facebook/get_my_score.php", variables, onMybestScoreComplete, onMybestScoreError);
		}
		
		private function onMybestScoreComplete(event:Event):void
		{
			trace("onMybestScoreComplete: " + event.target.data);
			var variables:URLVariables = new URLVariables(event.target.data);
			
			for (var property:String in variables)
			{
				trace(property + " = " + variables[property]);
			}
			
			if (int(variables.score) > 0)
			{
				cbGetMyBestScore(int(variables.score));
			}
			else
			{
				cbGetMyBestScore(0);
			}
		
		}
		
		private function onMybestScoreError():void
		{
			trace("onMybestScoreError: ");
			cbGetMyBestScore(0);
		}
		
		/*********************************************************************************
		 * Get all scores of all friends
		 *
		 * @param	pCBGetMyBestScore Callback functio with int parameter
		 */
		
		private function getAllScores():void
		{
			if (!connected || !isOnFacebook)
			{
				return;
			}
			trace("*** MyFacebook: getAllScores");
			
			var variables:URLVariables = new URLVariables();
			
			for (var i:String in arrFriendList)
			{
				variables["u" + i] = arrFriendList[i].uid;
			}
			variables.nbUsers = i;
			
			var save:MyTextLoader = new MyTextLoader("http://www.benoitfreslon.com/modules/facebook/get_all_scores.php", variables, onAllScoresComplete, onAllScoresError);
		}
		
		private function onAllScoresComplete(event:Event):void
		{
			trace("*** MyFacebook: onAllScoresComplete: " + event.target.data);
			if (arrFriendList.length == 0)
			{
				return
			}
			
			var variables:URLVariables = new URLVariables(event.target.data);
			for (var i:int = 0; i < variables.nbUsers; i++)
			{
				trace(i, "->", variables["uid" + i], variables["score" + i]);
				
				for (var s:String in arrFriendList)
				{
					if (arrFriendList[s].uid == variables["uid" + i])
					{
						arrFriendList[s].score = int(variables["score" + i]);
						break;
					}
				}
			}
		}
		
		private function onAllScoresError():void
		{
			trace("*** MyFacebook: onAllScoresError");
		}
		
		/*********************************************************************************
		 * Get friends
		 *
		 * @param	pOnGetFriends
		 */
		public function getFriends(pOnGetFriends:Function=null):void
		{
			//Facebook.api("/me/friends", pOnGetFriends, null, "GET");
			cbGetFriends = pOnGetFriends;
			Facebook.fqlQuery("select uid, name, first_name, pic_square from user where is_app_user = 1 and uid in (SELECT uid2 FROM friend WHERE uid1 = me())", onGetFriends);
		}
		
		private function onGetFriends(result:Object, fail:Object):void
		{
			if (result)
			{
				trace("*** MyFacebook: onGetFriends RESULT:" + JSON.encode(result));
					//trace("*** MyFacebook: onCallApi RESULT:"); 
			}
			else
			{
				trace("*** MyFacebook: onGetFriends FAIL :" + JSON.encode(fail));
				//trace("*** MyFacebook: onCallApi FAIL :");
				return;
			}
			
			for (var s:String in result)
			{
				result[s].compared = false;
				arrFriendList.push(result[s]);
				
				Debug.object(result[s]);
			}
			if (cbGetFriends != null) {
				cbGetFriends(arrFriendList);
			}
			getAllScores();
		}
		
		/**********************************************************************************
		 * Compare score with freinds
		 *
		 */
		private function compareScore():void
		{
			trace("*** MyFacebook: compareScore",arrFriendList.length);
			var arr:Array = [];
			
			for (var s:String in arrFriendList)
			{
				trace("compare : ",arrFriendList[s].score, userBestScore)
				if (arrFriendList[s].score < userBestScore && arrFriendList[s].compared == false )
				{
					arr.push(arrFriendList[s]);
				}
			}
			if (arr.length > 0)
			{
				showFriendHUD(MyArray.selectRandom(arr));
			}
		}
		
		private function showFriendHUD(friend:Object):void
		{
			trace("*** MyFacebook: showFriendHUD");
			
			friend.compared = true;
			
			if (hud) {
				HUDRemove();
			}
			
			hud = new MovieClip();
			
			hud.friend = friend
			
			hud.w = 230;
			hud.h = 60;
			
			var black:MovieClip = new MovieClip();
			
			var matrixRotate90:Matrix = new Matrix();
			
			var fType:String = GradientType.LINEAR;
			var colors:Array = [0xEEEEEE, 0xAAAAAA];
//Store the Alpha Values in the form of an array
			var alphas:Array = [1, 1];
//Array of color distribution ratios.  
//The value defines percentage of the width where the color is sampled at 100%
			var ratios:Array = [0, 255];
//Create a Matrix instance and assign the Gradient Box
			var matr:Matrix = new Matrix();
			matr.createGradientBox(200, 20, 0, 0, 0);
//SpreadMethod will define how the gradient is spread. Note!!! Flash uses CONSTANTS to represent String literals
			var sprMethod:String = SpreadMethod.PAD;
			black.graphics.beginGradientFill(fType, colors, alphas, ratios, matr, sprMethod);
			//black.alpha = 0.8;
			black.rotation = 90;
			black.x = hud.w;
			black.graphics.drawRect(0, 0, hud.h, hud.w);
			black.graphics.endFill();
			hud.addChild(black);
			hud.mouseEnabled = true;
			
			var loaderImage:Loader = new Loader();
// url de l'image ou du swf à charger
			var image:URLRequest = new URLRequest(friend.pic_square);
			loaderImage.load(image);
			loaderImage.x = hud.w - (50 + 5);
			loaderImage.y = 5;
			hud.addChild(loaderImage);
			hud.x = mc.stage.stageWidth - hud.w;
			hud.y = mc.stage.stageHeight - hud.h;
			;
			
			mc.addChild(hud);
			
			var txt:TextField = new TextField();
			hud.txt = txt;
//txt.backgroundColor = 0x0000;
			txt.x = 2;
			txt.y = 2;
			txt.width = hud.w - (50 + 10);
			txt.border = false;
			txt.background = false;
			txt.multiline = true;
			txt.wordWrap = true;
			txt.selectable = false;
			txt.textColor = 0x333333
//txt.borderColor = 0xFFFF00;
			txt.htmlText = "<p align='right'><font size='9' face='Verdana'><b>"+Localization["fb_congratulations"]+"</b><br />"+Localization["fb_youBeatTheScore"]+" <b>" + friend.first_name + "</b>: " +friend.score+"pts <br /><font color='#F49629'><b>"+Localization["fb_clickHereToPost"]+"</b></font></p>";
			
			txt.autoSize = "right";
			
			hud.mouseChildren = false;
			hud.addChild(txt);
			hud.nextY = hud.y;
			hud.y = hud.nextY + hud.h
			hud.alpha = 0;
			hud.buttonMode = true;
			TweenMax.to(hud, 1, {y: hud.nextY, alpha: 1});
			TweenMax.to(hud, 1, {delay: 5, y: hud.nextY + hud.h - 15});
			TweenMax.delayedCall(10, HUDRemove);
			
			hud.addEventListener(MouseEvent.CLICK, HUDClick);
			hud.addEventListener(MouseEvent.MOUSE_OVER, HUDMouseOver);
		}
		
		private function HUDClick(e:MouseEvent):void 
		{
			postOnUserWall(hud.friend.uid, Localization["fb_iJustBeatMyScore2"]+" " + userBestScore + " "+Localization["fb_pts"]+". "+Localization["fb_canYouBeatMe"]);
			hud.txt.htmlText = "<p align='right'><font size='9' face='Verdana'><b>"+Localization["fb_posted"]+"</b></font></p>"
			TweenMax.to(hud, 2, { alpha:0, onComplete:HUDRemove } );
			hud.mouseEnabled = false;
		}
		
		private function HUDMouseOver(e:MouseEvent):void {
			TweenMax.to(hud, 1, {y: hud.nextY, alpha: 1});
			TweenMax.to(hud, 1, {delay: 2, y: hud.nextY + hud.h - 15});
		}
		private function HUDRemove():void {
			if (hud && mc.contains(hud)) {
				mc.removeChild(hud);
				hud = null;
			}
		}
		/**********************************************************************************
		 * Destroy
		 *
		 */
		private function destroy():void
		{
			trace("*** MyFacebook: destroy");
			
			SoundMixer.stopAll();
			SoundMixer.soundTransform = new SoundTransform(0, 0);
			
			var i:int = mc.numChildren;
			while (i--)
			{
				//mc.removeChildAt(0);
				mc.visible = false;
			}
			
			//mc.gotoAndStop(1);
			var black:MovieClip = new MovieClip();
			black.graphics.beginFill(0x000000);
			black.graphics.drawRect(0, 0, 10000, 10000);
			black.graphics.endFill();
			mc.addChild(black);
			
			var txt:TextField = new TextField();
			txt.backgroundColor = 0x0000;
			txt.border = true;
			txt.background = true;
			txt.selectable = false;
			txt.textColor = 0xFFFFFF;
			txt.borderColor = 0xFFFFFF;
			txt.htmlText = "<p align='center'><font size='20' face='Verdana'><a href='" + appUrl + "'><b>Unautorized domain <br/> CLICK TO PLAY</b></a></p>";
			//txt.autoSize = "center";
			//txt.buttonMode = true;
			txt.width = mc.stage.stageWidth;
			txt.height = mc.stage.stageHeight;
			
			txt.autoSize = "center"
			//trace(mc.stage.stageWidth);
			mc.stage.scaleMode = StageScaleMode.NO_BORDER;
			txt.x = 0;
			txt.y = 0;
			mc.buttonMode = true;
			mc.stage.addChild(txt);
			mc.stage.addEventListener(MouseEvent.CLICK, gotoWebsite)
		}
		
		private function gotoWebsite(pEvt:MouseEvent):void
		{
			var request:URLRequest = new URLRequest(appUrl);
			try
			{
				navigateToURL(request, "_blank");
			}
			catch (error:Error)
			{
			}
		}
		
		/********************************************************************************
		 * Load language
		 */
		protected function loadLanguage():void
		{
			//trace("MyMain: loadLanguage");
			xmlLanguage = null;
			/*
			   var variables:URLVariables = new URLVariables();
			   if (urlLanguage.split("://").length == 2) {
			   variables.t = int(Math.random()*10000000);
			   }
			 */
			//try {
			Security.loadPolicyFile("http://www.benoitfreslon.com/crossdomain.xml");
			languageLoader.load(URL_LANGUAGE, null, languageXMLLoaded, languageError);
			//} catch (e) {
			//languageError();
			//}
		}
		
		public function languageXMLLoaded(pEvt:Event):void
		{
			try
			{
				xmlLanguage = new XML(pEvt.target.data); //
				arrLanguage = [];
				for each (var xml:XML in xmlLanguage.children())
				{
					//trace("xmlll", xml.@language, xml.name(),xml.localName);
					if (xml.@language != "" && xml.name() != "Hello")
					{
						arrLanguage.push({code: xml.name(), language: xml.@language});
					}
				}
				languageLoaded = true;
				setLanguage(Capabilities.language);
			}
			catch (e:Error)
			{
				trace("Error language.xml on server " + e);
				languageError();
			}
		}
		
		public function languageError(pEvt:Event = null):void
		{
			xmlLanguage = XML(new xmlLanguageBackup());
		
		}
		
		public function setLanguage(pLanguage:String):Object
		{
			trace("*** MyFacebook : setLanguage", pLanguage);
			language = pLanguage;
			
			xmlLanguage.ignoreWhitespace = true;
			xmlLanguage.ignoreComments = true;
			// English language by default
			var langEn:XMLList = xmlLanguage.child("en").children();
			
			//var obj:Dictionary = new Dictionary();;
			var xml:XML;
			for each (xml in langEn)
			{
				Localization["fb_" + xml.name()] = xml;
			}
			
			var lang:XMLList = xmlLanguage.child(pLanguage).children();
			for each (xml in lang)
			{
				Localization["fb_" + xml.name()] = xml;
			}
			//trace("lang"+lang.length());
			if (lang.length() == 0)
			{
				language = "en";
			}
			
			return Localization;
		}
		
		public function postOnMyWall():void 
		{
			var obj = { };
			obj.picture = appIcon;
			obj.link = appUrl;
			obj.caption = Localization["fb_justBeatMyScore"]+" " + userBestScore + Localization["fb_pts"];
			obj.name = appName;
			obj.description = appDescription;
			Debug.object(obj);
			callUI("feed", obj);
		}
	}
}
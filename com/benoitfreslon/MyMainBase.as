﻿package com.benoitfreslon {
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;
	import flash.net.navigateToURL;
	import flash.net.SharedObject;
	import flash.net.URLRequest;
	import flash.system.Capabilities;
	
	/**
	 * ...
	 * @author Benoît Freslon
	 */
	public class MyMainBase extends MyMovieClip {
		
		//////////////////////////////////////////////////////////////
		// VARIABLES TO SET
		public var version:int = 1;
		public var gameNameCopy:String = "Replace by your game name";
		
		public var isTestMode : Boolean = false;
		public var isDebugMode :Boolean = false;
		public var isCheatMode :Boolean = false;
		
		public var socialMessage : String;
		public var twitterHashtag : String;
		public var gameURL : String;
		public var moreGames : String;
		public var website : String;
		public var gameNameCopyrights : String;
		public var cookieName : String;
		public var activeAds : Boolean = false;
		public var urlLanguage:String;
		//////////////////////////////////////////////////////////////
		
		public var curlanguage : String = "en";
		public var arrAllLanguage : Array = [];
		public var isMuted : Boolean = false;
		public var languageURL : String;
		public var so : Object = {data: {} , flush: function () : void {
		}};
		
		protected var _curPage : String;
		
		protected var _embedXML:XML;
		protected var _isLanguageLoaded:Boolean = false;
		protected var _isGameLoaded : Boolean = false;
		protected var _languageXML : XML = new XML ();
		protected var _languageLoader : MyTextLoader = new MyTextLoader ();
		
		public function MyMainBase () {
			stop ();
			super ();
			if (stage)
				init ();
			else
				addEventListener (Event.ADDED_TO_STAGE , init);
		}
		
		protected function init (e : Event = null) : void {;
			trace ("***********************************************************************");
			trace ("******************* " , gameNameCopy , version);
			trace ("***********************************************************************");
		}
		
		////////////////////////////////////////////////////////////// LOADING
		protected function pageLoading () : void {
			stop ();
			addEventListener (Event.ENTER_FRAME , loading);
			if (loaderInfo.bytesLoaded == loaderInfo.bytesTotal) {
				_isGameLoaded = true;
			} else {
				loaderInfo.addEventListener (Event.COMPLETE , loaded);
			}
		}
		
		private function loaded (e : Event) : void {
			_isGameLoaded = true;
		}
		
		protected function loading (e : Event) : void {
			//trace(cookieLoaded,xmlLanguage != null,gameLoaded);
			if (_languageXML != null && _isGameLoaded) {
				allIsLoaded ();
				removeEventListener (Event.ENTER_FRAME , loading);
			}
		}
		
		protected function allIsLoaded () : void {
			trace ("MyMainBase: allIsLoaded");
		}
		
		////////////////////////////////////////////////////////////// BUTTONS
		public function clickWebsite (e : MouseEvent = null) : void {
			try {
				var request : URLRequest = new URLRequest (website);
				navigateToURL (request , "_blank");
			} catch (error : Error) {
				
			}
		}
		
		public function clickMoreGames (e : MouseEvent = null) : void {
			try {
				var request : URLRequest = new URLRequest (moreGames);
				navigateToURL (request , "_blank");
			} catch (error : Error) {
				
			}
		}
		
		public function clickHelpMe (e : MouseEvent = null) : void {
			try {
				var request : URLRequest = new URLRequest (languageURL);
				navigateToURL (request , "_blank");
			} catch (error : Error) {
				//trace("Error goto help me");
			}
		}
		
		public function clickPostOnFacebook (e : MouseEvent = null) : void {
			postOnFacebook (socialMessage, gameURL);
		}
		
		public function clickPostOnTwitter (e : MouseEvent = null) : void {
			postOnTwitter (socialMessage, gameURL);
		}
		
		////////////////////////////////////////////////////////////// COOKIE
		protected function getLocalCookie () : void {
			so = SharedObject.getLocal (cookieName ,"/");
		}
		
		////////////////////////////////////////////////////////////// LANGUAGE
		protected function loadLanguage (embedClass:Class = null) : void {
			trace ("MyMain: loadLanguage " + languageURL,embedClass);
			_isLanguageLoaded = false;
			_languageXML = null;
			
			if (embedClass != null) {
				_embedXML = new XML (new embedClass());
				loadLocalLanguage ();
			} else {
				_languageLoader.load (languageURL , null , languageXMLLoaded , languageError);
			}
		}
		
		public function languageXMLLoaded (e : Event) : void {
			//trace( "MyMain: languageXMLLoaded", pEvt.target.data );
			try {
				loadLanguageXML (new XML (e.target.data))
			} catch (e) {
				trace ("Error language.xml on server " + e);
				languageError ();
			}
			_languageLoader = null;
		}
		
		public function loadLanguageXML (xmlLanguage : XML) : void {
			//trace('MyMain: loadLanguageXML', xmlLanguage);
			_languageXML = xmlLanguage;
			_languageXML.ignoreWhitespace = true;
			_languageXML.ignoreComments = true;
			arrAllLanguage = ["en"];
			for each (var xml : XML in _languageXML.children ()) {
				if (xml.@language != "" && xml.name () != "Hello") {
					if (arrAllLanguage.indexOf (xml.name ()) == -1 && xml.name () != "en") {
						trace()
						arrAllLanguage.push (String (xml.@code));
					}
				}
			}
			setLanguage (Capabilities.language);
			_isLanguageLoaded = true;
		}
		
		protected function languageChanged () : void {
		
		}
		
		public function languageError (e : Event = null) : void {
			errorMessage ("Language error, load the embed file");
			loadLocalLanguage ();
			_languageLoader = null;
		}
		
		public function setLanguage (langCode : String , type : String = "code" , def : String = "en") : void {
			trace ("MyMain: setLanguage" , langCode , type , def);
			
			curlanguage = langCode;
			
			_languageXML.ignoreWhitespace = true;
			_languageXML.ignoreComments = true;
			
			// Language by default
			var langDef : XMLList;
			if (type == "code") {
				langDef = _languageXML.lang.(@code == def).children ();
			} else if (type == "representation") {
				def = "en_EN";
				langDef = _languageXML.lang.(@representation == def).children ();
			}
			
			var xml : XML;
			for each (xml in langDef) {
				Localization[xml.@k] = xml;
			}
			
			var lang : XMLList;
			if (type == "code") {
				lang = _languageXML.lang.(@code == langCode).children ();
			} else if (type == "representation") {
				for each (xml in _languageXML.children ()) {
					if (xml.@representation == langCode) {
						lang = xml.children ();
					}
				}
			}
			
			for each (xml in lang) {
				Localization[xml.@k] = xml;
			}
			
			if (!lang || lang.length () == 0) {
				trace ("No language found");
				curlanguage = def;
			}
			languageChanged ();
		}
		
		protected function loadLocalLanguage () : void {
			trace ("MyMain: loadLocalLanguage");
			loadLanguageXML (_embedXML);
		}
		
		protected function initLanguageSelection (mc : MovieClip) : void {
		}
		////////////////////////////////////////////////////////////// DEBUG
		protected function errorMessage(pError:String):Error {
			var err:Error = new Error(pError);
			err.getStackTrace();
			return err
		}
		////////////////////////////////////////////////////////////// SOUND
		public function mute () : void {
			SoundMixer.soundTransform = new SoundTransform (0);
			MySound.mute ();
			isMuted = true;
		}
		
		public function unMute () : void {
			SoundMixer.soundTransform = new SoundTransform (1);
			MySound.unmute ();
			isMuted = false;
		}
		
		////////////////////////////////////////////////////////////// SOCIAL
		public function postOnTwitter (msg : String , pURL : String = null , pImage : String = null) : void {
			
			var url : String = gameURL;
			if (pURL != null) {
				url = pURL;
			}
			var tweet : String = unescape (msg);
			try {
				var request : URLRequest = new URLRequest ("https://twitter.com/intent/tweet?url=" + url + "&text=" + tweet + "&hashtags=" + twitterHashtag);
				navigateToURL (request);
			} catch (error : Error) {
				
			}
		}
		
		public function postOnFacebook (msg : String , pURL : String = null , pImage : String = null) : void {
			try {
				var request : URLRequest = new URLRequest ("http://www.facebook.com/share.php?u=" + gameURL);
				navigateToURL (request , "_blank");
			} catch (error : Error) {
				
			}
		}
		
		////////////////////////////////////////////////////////////// PAGES
		public function changePage (nextPage : String) : void {
			//trace("Main: changePage",pNextPage);
			_curPage = nextPage;
			gotoAndStop2 (_curPage);
		}
		
		public function transitionEnd () : void {
			//trace("Main: transitionEnd",nextPage);			
			gotoAndStop2 (_curPage);
		}
		
		public function gotoAd (e : MouseEvent = null) : void {
			changePage ("ad");
		}
		
		public function gotoDev (e : MouseEvent = null) : void {
			changePage ("dev");
		}
		
		public function gotoHiscores (e : MouseEvent = null) : void {
			changePage ("hiscores");
		}
		
		public function gotoTutorial1 (e : MouseEvent = null) : void {
			changePage ("tutorial1");
		}
		
		public function gotoTutorial2 (e : MouseEvent = null) : void {
			changePage ("tutorial2");
		}
		
		public function gotoMenu (e : MouseEvent = null) : void {
			changePage ("menu");
		}
		
		public function gotoMode (e : MouseEvent = null) : void {
			changePage ("mode");
		}
		
		public function gotoGameover (e : MouseEvent = null) : void {
			changePage ("gameover");
		}
		
		public function gotoCredits (e : MouseEvent = null) : void {
			changePage ("credits");
		}
		
		public function gotoGame (e : MouseEvent = null) : void {
			changePage ("game");
		}
	}

}
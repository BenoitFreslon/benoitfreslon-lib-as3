﻿package com.benoitfreslon.fx
{
	
	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.01
	 */
	
	import flash.display.DisplayObjectContainer
	import flash.display.Sprite;
	import flash.geom.ColorTransform;
	import flash.events.Event;

	public class Blink extends Sprite
	{
		
		private var mc:DisplayObjectContainer;
		private var t:int = 0;
		private var time:int = 1;
		public var color:int = 0xFFFFFF;
		
		public function Blink(pMc:DisplayObjectContainer):void {
			//trace("*** Blink: Constructor", pMc, pTime);
			mc = pMc;

			mc.addChild(this);
		}
		public function start(pTime:int = 1, pColor:int = 0xFFFFFF):void {
			stop();
			normal();
			
			time = pTime * 2;
			color = pColor;
			t = 0;
			//trace("*** Blink: start");
			white();
			addEventListener(Event.ENTER_FRAME, enterFrame);
			mc.addEventListener(Event.REMOVED_FROM_STAGE, removed);
			
		}
		private function enterFrame(pEvt:Event):void {
			t++
			if (t == time) {
				stop();
			} else if (t%2 == 0){
				white()
			} else {
				normal();
			}
		}
		private function white():void {
			//trace("*** Blink: white");
			var cTint:ColorTransform = new ColorTransform();
			cTint.color = color;
			mc.transform.colorTransform = cTint;
		}
		private function normal():void 
		{
			//trace("*** Blink: normal");
			var cTint:ColorTransform = new ColorTransform();
			mc.transform.colorTransform = cTint;
		}
		public function stop():void {
			//trace("*** Blink: stop");
			normal();
			removeEventListener(Event.ENTER_FRAME, enterFrame);
			mc.removeEventListener(Event.REMOVED_FROM_STAGE, removed);
		}
		private function removed(pEvt:Event ):void 
		{
			//trace("*** Blink: removed");
			stop();
		}
	}
	
}
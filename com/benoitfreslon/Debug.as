﻿package com.benoitfreslon {
	
	/**
	 * @private
	 * @author Benoît Freslon
	 * @exampleText Easy debug
	 * @version 1.07
	 */
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.text.TextField;
	import flash.events.Event;
	import flash.utils.setInterval;
	import flash.text.TextFormat;
	import net.hires.debug.Stats;
	
	public class Debug {
		
		static public var enabled:Boolean = true;
		
		static public function log( ... rest ):void {
			if ( !enabled )
				return;
			var error:Error = new Error();
			
			var tr:String = "";
			for each (var msg:String in rest)
			{
				tr +=  msg + " ";
			}
			var s:String = error.getStackTrace();
			
			var arr:Array = s.split("at ")[2].split("()");
			
			var stack:String = arr[0];
			var arrClass:Array = stack.split("/")[0].split("::");
			var className:String = arrClass[arrClass.length - 1];
			var functionName:String = stack.split("/")[1];
			if ( functionName == null )
			{
				functionName = className;
			}
			var arrLine:Array = arr[1].split(":");
			if (arrLine.length > 1) {
				var line:String = arrLine[2].split("]")[0]
				trace( "[" + className + "][" + functionName + "]" + "[" + line + "]", tr );;
			} else {
				trace( "[" + className + "][" + functionName + "]", tr );
			}
		}
		
		/**
		 * Display the framerate on the top left corner of a MovieClip
		 *
		 * @param FPS container
		 */
		
		public static function showFPS( pMc:Stage, pColor:uint=0x000000 ):void {
			trace( "*** Debug : Show FPS" );
			var txtFPS:TextField = new TextField();
			txtFPS.mouseEnabled = false;
			txtFPS.autoSize = "left";
			txtFPS.htmlText
			txtFPS.textColor = pColor
			
			var t:int = 0;
			setInterval( displayFPS, 1000, txtFPS );
			function FPScounter( pEvt:Event ):void {
				t++;
			}
			//pMc.addChild(txtFPS);
			txtFPS.addEventListener( Event.ENTER_FRAME, FPScounter );
			function displayFPS():void {
				txtFPS.htmlText = String( t ) + "fps";
				pMc.addChild( txtFPS );
				t = 0;
			}
		}
		
		public static function showStats( pStage:Stage ):Stats {
			var stats:Stats = new Stats();
			stats.mouseEnabled = false;
			pStage.addChild( stats );
			return stats
		}
		
		public static function object( pObj:Object ):void {
			trace( "\t### Object ###" );
			for ( var i:*in pObj ) {
				trace( "\t#", i, "\t=", pObj[ i ] );
			}
			trace( "\t###### End Object" );
		}
		
		public static function array( pArr:Array ):void {
			trace( "\t### Array ###" );
			for ( var i:String in pArr ) {
				trace( "\t# " + i + "\t > " + pArr[ i ] );
			}
			trace( "\t### End Array ###" );
		}
		
		public static function arrayObject( pArr:Array, pObject:String ):void {
			trace( "\t### Array Object ###" );
			for ( var i:String in pArr ) {
				trace( "\t#", i, "=", pArr[ i ][ pObject ] );
			}
			trace( "\t###### End Array Object" );
		}
		
		public static function movieClip( pMc:MovieClip ):void {
			trace( "\t###", pMc, pMc.name, "###" );
			trace( "\t# x \t\t=", pMc.x );
			trace( "\t# y \t\t=", pMc.y );
			trace( "\t# width \t=", pMc.width );
			trace( "\t# height \t=", pMc.height );
			trace( "\t# alpha \t=", pMc.alpha );
			trace( "\t# scaleX \t=", pMc.scaleX );
			trace( "\t# scaleY \t=", pMc.scaleY );
			trace( "\t# rotation \t=", pMc.rotation );
			trace( "\t# container \t=", pMc.parent );
			trace( "\t### End MovieClip" );
		}
		public static function container (d:DisplayObjectContainer): void {
			trace ("\t###",d, d.name, "###");
			for (var i: int = 0 ; i < d.numChildren;i++ ) {
				trace ("\t#", d.getChildAt (i),d.getChildAt (i).name);
			}
		}
	}
}
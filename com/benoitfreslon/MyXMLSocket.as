﻿package com.benoitfreslon
{
	import flash.net.XMLSocket
    import flash.events.*;

	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 0.11
	 */
	
	public class MyXMLSocket
	{
		private var socket:XMLSocket;
		public var host:String;
		public var port:uint;
		public var fcConnect:Function = null;
		public var fcData:Function = null;
		public var fcClose:Function = null;
		public var fcIOError:Function = null;
		public var fcSecurityError:Function = null;
		
        public function MyXMLSocket() {
            socket = new XMLSocket();
            configureListeners(socket);
			
        }
		public function get connected():Boolean {
			return socket.connected;
		}
		public function connect(pHost:String, pPort:uint):void {
			host = pHost;
			port = pPort;
			socket.connect(host, port);
		}
		public function close():void {
			if (socket.connected) {
				socket.close();
			}
		}
        public function send(pString:String):void {
			trace("MyXMLSocket: send:", pString);
            socket.send(new XML(pString));
        }

        private function configureListeners(dispatcher:IEventDispatcher):void {
            dispatcher.addEventListener(Event.CLOSE, closeHandler);
            dispatcher.addEventListener(Event.CONNECT, connectHandler);
            dispatcher.addEventListener(DataEvent.DATA, dataHandler);
            dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
            dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
            dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
        }

        private function closeHandler(event:Event):void {
            trace("closeHandler: " + event);
			if (fcClose != null) {
				fcClose(event);
			}
        }

        private function connectHandler(event:Event):void {
            //trace("connectHandler: " + event);
			if (fcConnect != null) {
				fcConnect(event);
			}
        }

        private function dataHandler(event:DataEvent):void {
			//trace("MyXMLSocket: dataHandler: " + event);
			if (fcData != null) {
				fcData(event);
			}
        }

        private function ioErrorHandler(event:IOErrorEvent):void {
            trace("ioErrorHandler: " + event);
			if (fcIOError != null) {
				fcIOError(event);
			}
        }

        private function progressHandler(event:ProgressEvent):void {
            trace("progressHandler loaded:" + event.bytesLoaded + " total: " + event.bytesTotal);
        }

        private function securityErrorHandler(event:SecurityErrorEvent):void {
            trace("securityErrorHandler: " + event);
			if (fcSecurityError != null) {
				fcSecurityError(event);
			}
        }

	}
	
}
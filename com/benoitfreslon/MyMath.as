﻿package com.benoitfreslon {
	
	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.08
	 */
	
	public final class MyMath {
		public static const PI_ON_2 : Number = 1.5707963267948966;
		public static const PI_BY_2 : Number = 6.283185307179586;
		public static const COEF_RAD_2_DEG : Number = 57.29577951308232;
		public static const COEF_DEG_2_RAD : Number = 0.017453292519943295;
		
		public static function getResAngle( angle1 : Number, angle2 : Number ) : Number {
			var a : Number = angle1 - angle2;
			if ( a > 180 ) {
				a = a - 360;
			} else if ( a < -180 ) {
				a = a + 360;
			}
			return a;
		}
		
		public static function ruleOfTree( pA : Number, pB : Number, pX : Number ) : Number {
			return ( pA * pX ) / pB;
		}
		
		public static function proportional( pMin : Number, pMax : Number, pPercent : Number ) : Number {
			return ( pMax - pMin ) * pPercent + pMin;
		}
		
		public static function randBool() : Boolean {
			if ( 0.5 < Math.random() ) {
				return true;
			} else {
				return false;
			}
		}
		
		public static function dice( pInt : int ) : int {
			return int( Math.random() * pInt );
		}
		
		public static function getResAngleRad( angle1 : Number, angle2 : Number ) : Number {
			var a : Number = angle1 - angle2;
			if ( a > Math.PI ) {
				a = a - MyMath.PI_BY_2;
			} else if ( a < ( -Math.PI ) ) {
				a = a + MyMath.PI_BY_2;
			}
			return a;
		}
		
		public static function randRange( min : Number, max : Number ) : Number {
			return min + ( Math.random() * ( max - min ) );
		}
		
		public static function threshold( pValue : Number, pMin : Number, pMax : Number ) : Number {
			return Math.max( Math.min( pValue, pMax ), pMin );
		}
		
		public static function pythagore( pValue1 : Number, pValue2 : Number ) : Number {
			return Math.sqrt( pValue1 * pValue1 + pValue2 * pValue2 );
		}
		
		public static function pythagore3( pValue1 : Number, pValue2 : Number, pValue3 : Number ) : Number {
			return Math.sqrt( pValue1 * pValue1 + pValue2 * pValue2 + pValue3 * pValue3 );
		}
		
		public static function sec2min( pSec : int, pChar : String = ":" ) : String {
			var min : int = pSec / 60;
			var sec : int = pSec % 60;
			stringSec = String( sec );
			if ( sec < 10 ) {
				var stringSec : String = "0" + sec;
			}
			return min + pChar + stringSec;
		}
		
		public static function number2String( pNumber : Number ) : String {
			var pos : Boolean = true;
			if ( pNumber < 0 ) {
				pos = false;
			}
			var string : String = Math.abs( pNumber ).toString();
			var integer : String = string.split( "." )[ 0 ]
			var decimal : String = string.split( "." )[ 1 ]
			var arrInteger : Array = [];
			for ( var i : int = 0; i < int( integer.length / 3 ); i++ ) {
				arrInteger.push( integer.slice( integer.length - 3 * ( i + 1 ), integer.length - 3 * i ) )
			}
			if ( integer.length > i * 3 ) {
				var s : String = ""
				if ( integer.charAt( 0 ) ) {
					s += integer.charAt( 0 )
				}
				if ( integer.charAt( 1 ) && integer.length - 2 >= i * 3 ) {
					s += integer.charAt( 1 )
				}
				if ( integer.charAt( 2 ) && integer.length - 3 >= i * 3 ) {
					s += integer.charAt( 2 )
				}
				arrInteger.push( s );
			}
			arrInteger.reverse();
			if ( pos ) {
				return arrInteger + "." + decimal
			} else {
				return "-" + arrInteger + "." + decimal
			}
		}
		
		public static function numberToScore( score : int ) : String {
			var number : String = String( score );
			var pos : Boolean = true;
			if ( score < 0 ) {
				pos = false;
			}
			var string : String = Math.abs( score ).toString();
			
			var arr : Array = string.split( "" );
			arr.reverse()
			if ( string.length <= 3 ) {
				return pos ? string : "-" + string;
			}
			var s : String = "";
			for ( var i : int = 0; i < arr.length; i++ ) {
				s += arr[ i ];
				if (( i + 1 ) % 3 == 0 && i != 0 ) {
					s += " ";
				}
			}
			arr = s.split( "" );
			arr.reverse();
			s = "";
			for ( i = 0; i < arr.length; i++ ) {
				s += arr[ i ];
			}
			
			return pos ? s : "-" + s;
		}
	}
}
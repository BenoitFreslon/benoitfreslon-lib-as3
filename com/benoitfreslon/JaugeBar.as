﻿package com.benoitfreslon {
	
	/**
	 * @private
	 * @author Benoît Freslon
	 * @version 1.1
	 */
	
	public class JaugeBar extends MyMovieClip {
		
		public var _value:Number = 0;
		
		function JaugeBar() {
			stop();
		}
		public function set value(pValue:int):void {
			_value = pValue;

			if (_value < 0) {
				_value = 0;
			} else if (_value > 100) {
				_value = 100;
			}
			this["bar"].scaleX = pValue / 100;
		}
		public function get value():Number {
			return _value;
		}
	}
}
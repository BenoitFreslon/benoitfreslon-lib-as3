package com.benoitfreslon
{
	import flash.display.MovieClip;
	
	/**
	 * ...
	 * @author Benoît Freslon
	 */
	public class MyTextInput extends MovieClip 
	{
		private var _text:String = "";
		public function set text(pText:String):void {
			_text = pText;
			this["txt"].text = _text;
		}
		public function get text():String {
			return _text;
		}
	}
	
}